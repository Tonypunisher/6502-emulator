# 6502 Emulator

a 6502 CPU emulator. This CPU is used on multiple platforms and especially on the NES.

this project is built with Rust and it is managed with a docker container.
this repo contains:
* a docker images into the `Docker/` folder.
* a `Makefile` to easily run commands into the docker container.
* the rust project managed with cargo.

## build and run the project

First, you have to build the docker image if you want to use the Makefile:

```shell
cd ./Docker/
docker build -t rust-tools .
```

> Note: this image contains the rust compiler + some tools like rustfmt and clippy.

the `Makefile` help you to build and run the code, here is the main command:

* `build`: compile the code with cargo (optimized version):
```shell
make build
```

* `run`: compile the code with cargo and run it:
```shell
make run
```

* `help`: display all available commands with a description:
```shell
make help
```