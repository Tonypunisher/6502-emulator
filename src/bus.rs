// module to emulate a bus connected to a 64kb RAM.
// a component like a CPU can use this bus struct to init and read / write the RAM.
pub struct Bus {
    pub ram: [u8; 64 * 1024],
}

// static Bus init function
impl Bus {
    pub fn init() -> Bus {
        Bus {
            ram: [0x00; 64 * 1024],
        }
    }
}

// Bus methods
impl Bus {
    pub fn read(&self, addr: u16) -> u8 {
        // the address range is between 0x0000 and 0xFFFF
        // no need to validate with a if because u8 are exatly in this range
        // just read the ram here
        self.ram[addr as usize]
    }
    pub fn write(&mut self, addr: u16, data: u8) {
        // the address range is between 0x0000 and 0xFFFF
        // no need to validate with a if because u8 are exatly in this range
        // just write the ram here
        self.ram[addr as usize] = data;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    /* it should init 64Kb (65536 bit) RAM with 0x00 value */
    fn test_bus_init_ram() -> Result<(), String> {
        let bus = Bus::init();

        for mem_address in 0..65535 {
            assert_eq!(bus.ram[mem_address], 0x00);
        }
        Ok(())
    }

    #[test]
    /* it should write 0xFF data to adress 0x0FFF in the RAM */
    fn test_bus_write_ram() -> Result<(), String> {
        let mut bus = Bus::init();
        bus.write(0x0FFF, 0xFF);
        assert_eq!(bus.ram[0x0FFF], 0xFF);
        Ok(())
    }

    #[test]
    /* it should read 0xFF data to adress 0xFFFF in the RAM */
    fn test_bus_read_ram() -> Result<(), String> {
        // before: init a bus and write into the ram
        let mut bus = Bus::init();
        bus.ram[0xFFFF as usize] = 0xFF;

        assert_eq!(bus.read(0xFFFF), 0xFF);
        Ok(())
    }
}
