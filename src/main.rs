mod bus;
mod cpu_6502;

fn main() {
    println!("##### 6502 CPU Emulator #####");
    // init the bus and the RAM
    let bus_with_ram = bus::Bus::init();
    let mut cpu = cpu_6502::CPU::init(bus_with_ram);

    /*
        *=$8000
        LDX #10
        STX $0000
        LDX #3
        STX $0001
        LDY $0000
        LDA #0
        CLC
        loop
        ADC $0001
        DEY
        BNE loop
        STA $0002
        NOP
        NOP
        NOP
    */
    let raw_program = String::from(
        "A2 0A 8E 00 00 A2 03 8E 01 00 AC 00 00 A9 00 18 6D 01 00 88 D0 FA 8D 02 00 EA EA EA",
    );
    cpu.load_program(raw_program);
    cpu.execute_program();
}
