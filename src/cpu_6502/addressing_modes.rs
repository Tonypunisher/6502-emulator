use super::CPU;

// implement cpu adressing modes
impl CPU {
    pub fn imp(&mut self) -> u8 {
        self.fetched = self.a;
        0
    }

    pub fn imm(&mut self) -> u8 {
        self.addr_abs = self.pc;
        self.pc += 1;
        0
    }

    pub fn zp0(&mut self) -> u8 {
        self.addr_abs = self.bus.read(self.pc) as u16;
        self.pc += 1;
        // with with logical AND we set the page address to 00
        // and 'copy' the offset because of FF = 1111
        self.addr_abs &= 0x00FF;
        0
    }

    pub fn zpx(&mut self) -> u8 {
        self.addr_abs = (self.bus.read(self.pc) + self.x) as u16;
        self.pc += 1;
        // with with logical AND we set the page address to 00
        // and 'copy' the offset because of FF = 1111
        self.addr_abs &= 0x00FF;
        0
    }

    pub fn zpy(&mut self) -> u8 {
        self.addr_abs = (self.bus.read(self.pc) + self.y) as u16;
        self.pc += 1;
        // with with logical AND we set the page address to 00
        // and 'copy' the offset because of FF = 1111
        self.addr_abs &= 0x00FF;
        0
    }

    pub fn abs(&mut self) -> u8 {
        let lo = self.bus.read(self.pc) as u16;
        self.pc += 1;
        let hi = self.bus.read(self.pc) as u16;
        self.pc += 1;

        self.addr_abs = (hi << 8) | lo;

        0
    }

    pub fn abx(&mut self) -> u8 {
        let lo = self.bus.read(self.pc) as u16;
        self.pc += 1;
        let hi = self.bus.read(self.pc) as u16;
        self.pc += 1;

        self.addr_abs = (hi << 8) | lo;
        self.addr_abs += self.x as u16;

        if (self.addr_abs & 0xFF00) != (hi << 8) {
            return 1;
        }

        0
    }

    pub fn aby(&mut self) -> u8 {
        let lo = self.bus.read(self.pc) as u16;
        self.pc += 1;
        let hi = self.bus.read(self.pc) as u16;
        self.pc += 1;

        self.addr_abs = (hi << 8) | lo;
        self.addr_abs += self.y as u16;

        if (self.addr_abs & 0xFF00) != (hi << 8) {
            return 1;
        }

        0
    }

    pub fn ind(&mut self) -> u8 {
        let ptr_lo = self.bus.read(self.pc) as u16;
        self.pc += 1;
        let ptr_hi = self.bus.read(self.pc) as u16;
        self.pc += 1;

        let ptr = (ptr_hi << 8) | ptr_lo;

        // simulate page boundary hardware bug
        if ptr_lo == 0x00FF {
            let addr_abs_lo = self.bus.read(ptr) as u16;
            let addr_abs_hi = self.bus.read(ptr & 0xFF00) as u16;

            self.addr_abs = (addr_abs_hi << 8) | addr_abs_lo;
        } else {
            let addr_abs_lo = self.bus.read(ptr) as u16;
            let addr_abs_hi = self.bus.read(ptr + 1) as u16;

            self.addr_abs = (addr_abs_hi << 8) | addr_abs_lo;
        }

        0
    }

    pub fn izx(&mut self) -> u8 {
        let ptr_hi = self.bus.read(self.pc);
        self.pc += 1;

        let addr_abs_lo = self.bus.read((ptr_hi + self.x) as u16 & 0x00FF) as u16;
        let addr_abs_hi = self.bus.read((ptr_hi + (self.x + 1)) as u16 & 0x00FF) as u16;

        self.addr_abs = (addr_abs_hi << 8) | addr_abs_lo;

        0
    }

    pub fn izy(&mut self) -> u8 {
        let ptr_hi = self.bus.read(self.pc);
        self.pc += 1;

        let addr_abs_lo = self.bus.read(ptr_hi as u16 & 0x00FF) as u16;
        let addr_abs_hi = self.bus.read((ptr_hi as u16 + 1) & 0x00FF) as u16;

        self.addr_abs = (addr_abs_hi << 8) | addr_abs_lo;
        self.addr_abs += self.y as u16;

        if (self.addr_abs & 0xFF00) != (addr_abs_hi << 8) {
            return 1;
        }

        0
    }

    pub fn rel(&mut self) -> u8 {
        self.addr_rel = self.bus.read(self.pc) as u16;
        self.pc += 1;

        if (self.addr_rel & 0x80) != 0 {
            self.addr_rel |= 0xFF00;
        }

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::*;
    use crate::bus::Bus;

    // ###### describe: addressing modes functions ######
    // addressing modes are here to retrieve the address of the operand for an instruction

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // we ignore the opcode to test adressing modes here
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should directly set fetched (data for next instruction)
    // with the content of the a register and return 0 (no additionnal cycles to run)
    fn test_cpu_adressing_imp() -> Result<(), String> {
        // CLI
        let mut cpu = init_test_program(0x58, [0x00, 0x00]);
        cpu.a = 0xFE;

        let additionnal_cycles_after_exec = cpu.imp();

        assert_eq!(cpu.fetched, 0xFE);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should run immediate mode, it point to the next address to use (pc++).
    // No additionnal cycles to run, return 0
    fn test_cpu_adressing_imm() -> Result<(), String> {
        // note program is init at 0x8000
        // LDA
        let mut cpu = init_test_program(0xA9, [0xAA, 0x00]);

        let additionnal_cycles_after_exec = cpu.imm();

        // after increment the pc due to opcode reading
        // pc is 0x8002
        assert_eq!(cpu.addr_abs, 0x8001);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from page 0 with next pc as offset
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_zero_page() -> Result<(), String> {
        // CMP
        let mut cpu = init_test_program(0xC5, [0xAA, 0x00]);

        let additionnal_cycles_after_exec = cpu.zp0();

        assert_eq!(cpu.addr_abs, 0x00AA);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from page 0 and offset it with x register content
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_zero_page_x() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xB5, [0xAA, 0x00]);
        // x register content
        cpu.x = 0x01;

        let additionnal_cycles_after_exec = cpu.zpx();

        assert_eq!(cpu.addr_abs, 0x00AB);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from page 0 and offset it with y register content
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_zero_page_y() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xB6, [0xAA, 0x00]);
        // y register content
        cpu.y = 0x01;

        let additionnal_cycles_after_exec = cpu.zpy();

        assert_eq!(cpu.addr_abs, 0x00AB);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_abs() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xAD, [0xAA, 0x01]);

        let additionnal_cycles_after_exec = cpu.abs();

        assert_eq!(cpu.addr_abs, 0x01AA);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // and offset it by the content of the X register
    // it return 0 (no additionnal cycles to run)
    // because it not pass to the next memory page.
    fn test_cpu_addressing_abs_x_no_next_memory_page() -> Result<(), String> {
        // CMP
        let mut cpu = init_test_program(0xDD, [0xAA, 0x01]);
        // x register offset
        cpu.x = 0x01;

        let additionnal_cycles_after_exec = cpu.abx();

        assert_eq!(cpu.addr_abs, 0x01AB);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // and offset it by the content of the X register
    // it return 1 (1 additionnal cycles to run)
    // because it pass to the next memory page.
    fn test_cpu_addressing_abs_x_next_memory_page() -> Result<(), String> {
        // CMP
        let mut cpu = init_test_program(0xDD, [0xFF, 0x01]);
        // x register offset
        cpu.x = 0x01;

        let additionnal_cycles_after_exec = cpu.abx();

        assert_eq!(cpu.addr_abs, 0x0200);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 1);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // and offset it by the content of the X register
    // it return 0 (no additionnal cycles to run)
    // because it not pass to the next memory page.
    fn test_cpu_addressing_abs_y_no_next_memory_page() -> Result<(), String> {
        // CMP
        let mut cpu = init_test_program(0xD9, [0xAA, 0x01]);
        // y register offset
        cpu.y = 0x01;

        let additionnal_cycles_after_exec = cpu.aby();

        assert_eq!(cpu.addr_abs, 0x01AB);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // and offset it by the content of the X register
    // it return 1 (1 additionnal cycles to run)
    // because it pass to the next memory page.
    fn test_cpu_addressing_abs_y_next_memory_page() -> Result<(), String> {
        // CMP
        let mut cpu = init_test_program(0xD9, [0xFF, 0x01]);
        // y register offset
        cpu.y = 0x01;

        let additionnal_cycles_after_exec = cpu.aby();

        assert_eq!(cpu.addr_abs, 0x0200);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 1);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // the next address here is a pointer to an over address
    // it should read it.
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_indirect() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xAA, [0xAA, 0x01]);
        // init other address (pointer) to addrress 0x01AA
        cpu.write_ram(0x01AA, 0x02);
        cpu.write_ram(0x01AB, 0xAE);

        let additionnal_cycles_after_exec = cpu.ind();

        assert_eq!(cpu.addr_abs, 0xAE02);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses
    // the next address here is a pointer to an over address
    // it should read it.
    // it check the hardware bug is correct, it should not pass to the next address page
    // it return 0 (no additionnal cycles to run)
    // because it not pass to the next memory page.
    fn test_cpu_addressing_indirect_bug_memory_page() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xAA, [0xFF, 0x01]);
        // init other address (pointer) to addrress 0x01FF
        cpu.write_ram(0x01FF, 0x02);
        // BUG: it will not pass to the next page.
        cpu.write_ram(0x0100, 0xAE);

        let additionnal_cycles_after_exec = cpu.ind();

        assert_eq!(cpu.addr_abs, 0xAE02);
        assert_eq!(cpu.pc, 0x8003);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses at the page address 0
    // offset by the x register
    // the next address here is a pointer to an over address
    // it should read it.
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_indirect_x() -> Result<(), String> {
        // CMP
        let mut cpu = init_test_program(0xC1, [0xAA, 0x01]);
        // init other address (pointer) to addrress 0x01AA
        cpu.write_ram(0x00AB, 0x02);
        cpu.write_ram(0x00AC, 0xAE);

        // x register offset
        cpu.x = 0x01;

        let additionnal_cycles_after_exec = cpu.izx();

        assert_eq!(cpu.addr_abs, 0xAE02);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses at the page address 0
    // the next address here is a pointer to an over address
    // it should read it and offset by y register.
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_indirect_y() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xB1, [0xAA, 0x01]);
        // init other address (pointer) to addrress 0x01AA
        cpu.write_ram(0x00AA, 0x02);
        cpu.write_ram(0x00AB, 0xAE);

        // x register offset
        cpu.y = 0x01;

        let additionnal_cycles_after_exec = cpu.izy();

        assert_eq!(cpu.addr_abs, 0xAE03);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses at the page address 0
    // the next address here is a pointer to an over address
    // it should read it and offset by y register.
    // it return 1 (1 additionnal cycles to run)
    fn test_cpu_addressing_indirect_y_next_address_page() -> Result<(), String> {
        // LDA
        let mut cpu = init_test_program(0xB1, [0xAA, 0x01]);
        // init other address (pointer) to addrress 0x01AA
        cpu.write_ram(0x00AA, 0xFF);
        cpu.write_ram(0x00AB, 0xAE);

        // x register offset
        cpu.y = 0x01;

        let additionnal_cycles_after_exec = cpu.izy();

        assert_eq!(cpu.addr_abs, 0xAF00);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 1);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses as relative address
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_relative() -> Result<(), String> {
        // BPL
        let mut cpu = init_test_program(0x10, [0x7F, 0x01]);

        let additionnal_cycles_after_exec = cpu.rel();

        assert_eq!(cpu.addr_rel, 0x007F);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    #[test]
    // it should get data from next pc adresses as relative address
    // and return an adress from page 0xFF if low bite address >= 0x80
    // it return 0 (no additionnal cycles to run)
    fn test_cpu_addressing_relative_other_page() -> Result<(), String> {
        // BPL
        let mut cpu = init_test_program(0x10, [0x80, 0x01]);

        let additionnal_cycles_after_exec = cpu.rel();

        assert_eq!(cpu.addr_rel, 0xFF80);
        assert_eq!(cpu.pc, 0x8002);
        assert_eq!(additionnal_cycles_after_exec, 0);

        Ok(())
    }

    // ###### /addressing modes functions ######
}
