use std::fmt;

use crate::bus::Bus;

mod addressing_modes;
mod instructions;
mod program;

// the status register is an 8 bits register
// containing all status for each flags
// this enum represent all flags for the status register
#[repr(u8)]
pub enum FLAGS6502 {
    C = (1 << 0), // Carry Bit
    Z = (1 << 1), // Zero
    I = (1 << 2), // Disable Interrupts
    D = (1 << 3), // Decimal Mode (unused in this implementation)
    B = (1 << 4), // Break
    U = (1 << 5), // Unused
    V = (1 << 6), // Overflow
    N = (1 << 7), // Negative
}

pub const STACK_ADDR: u16 = 0x0100;

pub struct CPU {
    // registers
    a: u8,
    x: u8,
    y: u8,
    stk_p: u8,
    pc: u16,
    status: u8,

    // the bus to communicate with the RAM
    bus: Bus,

    // internal attributes for debug and help
    addr_rel: u16,
    addr_abs: u16,
    fetched: u8,
    opcode: u8,

    // cycles counter
    cycles: u8,
}

impl fmt::Display for CPU {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let instruction = self.get_instruction(self.opcode);
        write!(
            f,
            "
            ----------
            6502
            ----------
            instruction: {} ({:#04X?})
            addressing mode: {}
            data: {:#06X?}
            ----------
            Registers
            ----------
            A: {:#04X?}
            X: {:#04X?}
            Y: {:#04X?}

            StkP: {:#04X?}
            PC: {:#06X?}
            Status:
            | N | V | U | B | D | I | Z | C |
            | {} | {} | {} | {} | {} | {} | {} | {} |
            ----------




            ",
            instruction.name,
            self.opcode,
            instruction.addrmode_name,
            self.fetched,
            self.a,
            self.x,
            self.y,
            self.stk_p,
            self.pc,
            self.get_flag(FLAGS6502::N),
            self.get_flag(FLAGS6502::V),
            self.get_flag(FLAGS6502::U),
            self.get_flag(FLAGS6502::B),
            self.get_flag(FLAGS6502::D),
            self.get_flag(FLAGS6502::I),
            self.get_flag(FLAGS6502::Z),
            self.get_flag(FLAGS6502::C)
        )
    }
}

// create the static init function
// it take a bus as parameter to connect a Bus with at least read/write function
// to interract with a RAM
impl CPU {
    pub fn init(bus: Bus) -> CPU {
        let mut new_cpu = CPU {
            // registers
            a: 0x00,
            x: 0x00,
            y: 0x00,
            stk_p: 0x00,
            pc: 0x0000,
            status: 0x00,

            // the bus to communicate with the RAM
            bus,

            // internal state attribute for debug
            addr_rel: 0x0000,
            addr_abs: 0x0000,
            fetched: 0x00,
            opcode: 0x00,

            cycles: 0,
        };

        new_cpu.reset();
        new_cpu
    }
}

// create bus communication internal functions to read / write the RAM
impl CPU {
    fn read_ram(&self, addr: u16) -> u8 {
        self.bus.read(addr)
    }

    fn write_ram(&mut self, addr: u16, data: u8) {
        self.bus.write(addr, data);
    }
}

// functions to execute a program already loaded to the RAM
impl CPU {
    fn clock(&mut self) {
        if self.cycles == 0 {
            // read opcode and increment pc
            self.opcode = self.read_ram(self.pc);

            // always set the unused flag to 1
            self.set_flag(FLAGS6502::U, true);

            self.pc += 1;

            let instruction = self.get_instruction(self.opcode);
            self.cycles = instruction.cycles;

            // execute address mode function to get the data
            let address_mode_f = instruction.addrmode;
            let additionnal_cycles_addressing_mode: u8 = address_mode_f(self);

            // execute the instruction
            let execute_f = instruction.execute;
            let additionnal_cycles_execute_instruction: u8 = execute_f(self);

            self.cycles +=
                additionnal_cycles_addressing_mode & additionnal_cycles_execute_instruction;

            // always set the unused flag to 1
            self.set_flag(FLAGS6502::U, true);

            println!("{}", self);
        }

        self.cycles -= 1;
    }
}

// create the signals function (reset, ...)
impl CPU {
    pub fn reset(&mut self) {
        // program counter start address
        let pc_abs_address = 0xFFFC;
        // read the pc address
        let lo = self.bus.read(pc_abs_address) as u16;
        let hi = self.bus.read(pc_abs_address + 1) as u16;

        self.pc = (hi << 8) | lo;

        //reset internal registers
        self.a = 0;
        self.x = 0;
        self.y = 0;
        self.stk_p = 0xFD;
        self.status = FLAGS6502::U as u8;

        // clear internal helper variables
        self.addr_rel = 0x0000;
        self.addr_abs = 0x0000;
        self.fetched = 0x00;
        self.opcode = 0x00;

        // reset takes time
        // all operation take 8 cpu cycles here
        self.cycles = 8;
    }

    #[allow(dead_code)]
    pub fn irq(&mut self) {
        // interrupt only if I status is not set
        if self.get_flag(FLAGS6502::I) == 0 {
            // add pc to the stack
            self.write_ram(
                STACK_ADDR + self.stk_p as u16,
                ((self.pc >> 8) & 0x00FF) as u8,
            );
            self.stk_p -= 1;
            self.write_ram(STACK_ADDR + self.stk_p as u16, (self.pc & 0x00FF) as u8);
            self.stk_p -= 1;

            // update the status
            // and add the status to the stack
            self.set_flag(FLAGS6502::B, true);
            self.set_flag(FLAGS6502::U, true);
            self.set_flag(FLAGS6502::I, true);
            self.write_ram(STACK_ADDR + self.stk_p as u16, self.status);
            self.stk_p -= 1;

            self.addr_abs = 0xFFFE;
            let lo = self.read_ram(self.addr_abs) as u16;
            let hi = self.read_ram(self.addr_abs + 1) as u16;
            self.pc = (hi << 8) | lo;

            self.cycles = 7;
        }
    }

    #[allow(dead_code)]
    pub fn nmi(&mut self) {
        // add pc to the stack
        self.write_ram(
            STACK_ADDR + self.stk_p as u16,
            ((self.pc >> 8) & 0x00FF) as u8,
        );
        self.stk_p -= 1;
        self.write_ram(STACK_ADDR + self.stk_p as u16, (self.pc & 0x00FF) as u8);
        self.stk_p -= 1;

        // update the status
        // and add the status to the stack
        self.set_flag(FLAGS6502::B, true);
        self.set_flag(FLAGS6502::U, true);
        self.set_flag(FLAGS6502::I, true);
        self.write_ram(STACK_ADDR + self.stk_p as u16, self.status);
        self.stk_p -= 1;

        self.addr_abs = 0xFFFA;
        let lo = self.read_ram(self.addr_abs) as u16;
        let hi = self.read_ram(self.addr_abs + 1) as u16;
        self.pc = (hi << 8) | lo;

        self.cycles = 8;
    }
}

// implement flags getter and setter to manipullate the status register
impl CPU {
    fn get_flag(&self, flag: FLAGS6502) -> u8 {
        if (self.status & flag as u8) > 0 {
            return 1_u8;
        }

        0_u8
    }
    fn set_flag(&mut self, flag: FLAGS6502, value: bool) {
        // if value boolean is true, set the flag to 1
        if value {
            self.status |= flag as u8;
        } else {
            self.status &= !(flag as u8);
        }
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::*;

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    // ###### describe: init function ######

    #[test]
    /* it should init cpu struct and each elements */
    fn test_cpu_init() -> Result<(), String> {
        // we need a bus to connect to at leat a RAM
        let bus = Bus::init();

        let cpu = CPU::init(bus);
        // check all cpu elements
        // note ::init call cpu.reset() method
        assert_eq!(cpu.a, 0x00);
        assert_eq!(cpu.x, 0x00);
        assert_eq!(cpu.y, 0x00);

        assert_eq!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.pc, 0x0000);

        // all status are 0 execept U flag
        // in base 2: 0010 0000
        // to hex    :  2    0
        assert_eq!(cpu.status, 0x20);

        // check debug elements
        assert_eq!(cpu.addr_rel, 0x0000);
        assert_eq!(cpu.addr_abs, 0x0000);
        assert_eq!(cpu.fetched, 0x00);
        assert_eq!(cpu.opcode, 0x00);

        Ok(())
    }

    // ###### /init function ######

    // ###### describe: bus communication functions ######

    #[test]
    // it should call the bus to read the RAM at a given address
    fn test_cpu_read_ram() -> Result<(), String> {
        // before: init a Bus and a CPU
        let mut bus = Bus::init();
        // init memory
        bus.write(0x00FF, 0x10);
        let cpu = CPU::init(bus);

        assert_eq!(cpu.read_ram(0x00FF), 0x10);

        Ok(())
    }

    #[test]
    fn test_cpu_write_ram() -> Result<(), String> {
        // before: init a Bus and a CPU
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // write to RAM
        cpu.write_ram(0x00FF, 0x10);

        assert_eq!(cpu.read_ram(0x00FF), 0x10);

        Ok(())
    }

    // ###### /bus communication function ######

    // ###### describe: execute a program dedicated function ######
    #[test]
    fn test_clock() -> Result<(), String> {
        // before: init a Bus and a CPU
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // program with one instruction:
        // *=$8000
        // LDX #10
        let program = String::from("A2 0A");
        cpu.load_program(program);

        // the reset take 8 cycle, execute clock to simulate the 8 cycles execution
        for _i in 0..8 {
            cpu.clock();
        }
        // check reset cycles are executed
        assert_eq!(cpu.cycles, 0);

        // first clock call, load the opcode, execute addressing mode and execute the instruction
        // this instruction take 2 cycles
        cpu.clock();
        assert_eq!(cpu.opcode, 0xA2);
        assert_eq!(cpu.addr_abs, 0x8001);
        assert_eq!(cpu.x, 0x0A);
        // total cycles minus 1
        assert_eq!(cpu.cycles, 1);

        // simulate the second cycle execution
        cpu.clock();
        // the state not moving
        assert_eq!(cpu.opcode, 0xA2);
        assert_eq!(cpu.addr_abs, 0x8001);
        assert_eq!(cpu.x, 0x0A);
        // cycle is zero, the CPU is ready to pass to the next instruction
        assert_eq!(cpu.cycles, 0);

        Ok(())
    }

    // ###### /program ######

    // ###### describe: signals functions ######

    #[test]
    /* it should init the CPU elements to known state */
    fn test_cpu_reset() -> Result<(), String> {
        // before: init a fake pc address in RAM via the bus
        let mut bus = Bus::init();
        // low byte pc
        bus.write(0xFFFC, 0xFF);
        // high byte pc
        bus.write(0xFFFD, 0xFE);

        let mut cpu = CPU::init(bus);
        cpu.reset();
        assert_eq!(cpu.a, 0x00);
        assert_eq!(cpu.x, 0x00);
        assert_eq!(cpu.y, 0x00);

        assert_eq!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.pc, 0xFEFF);

        // all status are 0 execept U flag
        // in base 2: 0010 0000
        // to u8    :   2    0
        assert_eq!(cpu.status, 0x20);

        // check debug elements
        assert_eq!(cpu.addr_rel, 0x0000);
        assert_eq!(cpu.addr_abs, 0x0000);
        assert_eq!(cpu.fetched, 0x00);
        assert_eq!(cpu.opcode, 0x00);

        Ok(())
    }

    #[test]
    // it should make an interruption
    fn test_cpu_irq() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x25, [0xA0, 0x00]);
        cpu.write_ram(0xFFFE, 0x00);
        cpu.write_ram(0xFFFF, 0x90);
        // be sure we can interupt the cpu
        cpu.set_flag(FLAGS6502::I, false);

        cpu.irq();

        // check we save pc on the stack
        // high byte at 0x01FD
        // low byte at 0x01FC
        // after the program init, the pc is at 0x8001
        assert_eq!(cpu.read_ram(0x01FD), 0x80);
        assert_eq!(cpu.read_ram(0x01FC), 0x01);

        // check if the statuc register in on the stack and is updated
        // it is writed just after the pc
        // NVUBDIZC is the order of the flags in the byte
        // the status have the value 00101100 = 0x34
        assert_eq!(cpu.read_ram(0x01FB), 0x34);
        assert_eq!(cpu.get_flag(FLAGS6502::B), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::U), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::I), 1);

        // check the pc has been reinitialized from 0xFFFE
        assert_eq!(cpu.pc, 0x9000);

        // check it set cycles to 7
        // irq reset the cycles counter and take 7 cycles to make his own operations
        assert_eq!(cpu.cycles, 7);
        Ok(())
    }

    #[test]
    // it should not make an interruption because the I flag is set
    fn test_cpu_no_irq() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x25, [0xA0, 0x00]);
        cpu.write_ram(0xFFFE, 0x00);
        cpu.write_ram(0xFFFF, 0x90);
        // be sure we can interupt the cpu
        cpu.set_flag(FLAGS6502::I, true);

        cpu.irq();

        // check we have not save pc on the stack
        // low byte at 0x01FD
        // high byte at 0x01FC
        assert_ne!(cpu.read_ram(0x01FD), 0x80);
        assert_ne!(cpu.read_ram(0x01FC), 0x01);

        // check if the statuc register in not on the stack and is not updated
        assert_ne!(cpu.read_ram(0x01FB), 0x2C);

        // check the flag was not updated
        assert_eq!(cpu.status, 0x24);

        // check the pc has not been reinitialized from 0xFFFE
        assert_ne!(cpu.pc, 0x9000);

        // check it not set cycles to 7
        assert_ne!(cpu.cycles, 7);
        Ok(())
    }

    #[test]
    // it should make an interruption, even the I flag is set
    fn test_cpu_nmi() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x25, [0xA0, 0x00]);
        cpu.write_ram(0xFFFA, 0x00);
        cpu.write_ram(0xFFFB, 0x90);
        // be sure we can interupt the cpu
        cpu.set_flag(FLAGS6502::I, true);

        cpu.nmi();

        // check we save pc on the stack
        // high byte at 0x01FD
        // low byte at 0x01FC
        // after the program init, the pc is at 0x8001
        assert_eq!(cpu.read_ram(0x01FD), 0x80);
        assert_eq!(cpu.read_ram(0x01FC), 0x01);

        // check if the status register in on the stack and is updated
        // it is writed just after the pc
        // NVUBDIZC is the order of the flags in the byte
        // the status have the value 00101100 = 0x34
        assert_eq!(cpu.read_ram(0x01FB), 0x34);
        assert_eq!(cpu.get_flag(FLAGS6502::B), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::U), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::I), 1);

        // check the pc has been reinitialized from 0xFFFA
        assert_eq!(cpu.pc, 0x9000);

        // check it set cycles to 8
        // irq reset the cycles counter and take 7 cycles to make his own operations
        assert_eq!(cpu.cycles, 8);
        Ok(())
    }

    // ###### /signals functions ######

    // utils function: init a CPU and reset
    fn init_test_program_flags() -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        cpu.reset();

        cpu
    }

    // ###### describe: status register setter and getters (flags) ######
    #[test]
    /* it should set the C flag */
    fn test_set_c_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::C, true);
        assert_eq!(cpu.status, 0x21);
        cpu.set_flag(FLAGS6502::C, false);
        assert_eq!(cpu.status, 0x20);

        Ok(())
    }

    #[test]
    /* it should set the Z flag */
    fn test_set_z_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::Z, true);
        assert_eq!(cpu.status, 0x22);
        cpu.set_flag(FLAGS6502::Z, false);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should set the I flag */
    fn test_set_i_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::I, true);
        assert_eq!(cpu.status, 0x24);
        cpu.set_flag(FLAGS6502::I, false);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should set the D flag */
    fn test_set_d_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::D, true);
        assert_eq!(cpu.status, 0x28);
        cpu.set_flag(FLAGS6502::D, false);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should set the B flag */
    fn test_set_b_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::B, true);
        assert_eq!(cpu.status, 0x30);
        cpu.set_flag(FLAGS6502::B, false);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should set the U flag */
    fn test_set_u_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::U, false);
        assert_eq!(cpu.status, 0x00);
        cpu.set_flag(FLAGS6502::U, true);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should set the V flag */
    fn test_set_v_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::V, true);
        assert_eq!(cpu.status, 0x60);
        cpu.set_flag(FLAGS6502::V, false);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should set the Z flag */
    fn test_set_n_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::N, true);
        assert_eq!(cpu.status, 0xA0);
        cpu.set_flag(FLAGS6502::N, false);
        assert_eq!(cpu.status, 0x20);
        Ok(())
    }

    #[test]
    /* it should get the N flag */
    fn test_get_n_flag() -> Result<(), String> {
        let mut cpu = init_test_program_flags();

        cpu.set_flag(FLAGS6502::N, true);
        let flag_status = cpu.get_flag(FLAGS6502::N);
        assert_eq!(cpu.status, 0xA0);
        assert_eq!(flag_status, 0x01);
        Ok(())
    }
    // ###### /status register ######
}
