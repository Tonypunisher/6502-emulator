extern crate hex;

use super::CPU;
use super::FLAGS6502;

fn parse_program_to_hex(program: String) -> Vec<u8> {
    let mut program_hex: Vec<u8> = Vec::new();
    let splitted_str = program.split(' ');

    for str_opcode in splitted_str {
        program_hex.push(hex::decode(str_opcode).unwrap()[0] as u8);
    }

    program_hex
}

impl CPU {
    pub fn load_program(&mut self, program: String) {
        let parsed_program = parse_program_to_hex(program);
        let mut n_offset: u16 = 0x8000;

        for program_elmt in parsed_program {
            self.write_ram(n_offset, program_elmt);
            n_offset += 1;
        }

        // Set Reset Vector
        self.write_ram(0xFFFC, 0x00);
        self.write_ram(0xFFFD, 0x80);

        // Reset
        self.reset();

        // specific print after reset
        print!(
            "
            ----------
            6502
            ----------
            instruction: --
            addressing mode: --
            data: --
            ----------
            Registers
            ----------
            A: {:#04X?}
            X: {:#04X?}
            Y: {:#04X?}

            StkP: {:#04X?}
            PC: {:#06X?}
            Status:
            | N | V | U | B | D | I | Z | C |
            | {} | {} | {} | {} | {} | {} | {} | {} |
            ----------




            ",
            self.a,
            self.x,
            self.y,
            self.stk_p,
            self.pc,
            self.get_flag(FLAGS6502::N),
            self.get_flag(FLAGS6502::V),
            self.get_flag(FLAGS6502::U),
            self.get_flag(FLAGS6502::B),
            self.get_flag(FLAGS6502::D),
            self.get_flag(FLAGS6502::I),
            self.get_flag(FLAGS6502::Z),
            self.get_flag(FLAGS6502::C)
        );
        println!("program loaded and CPU ready to run");
    }

    fn is_complete(&self) -> bool {
        if self.cycles == 0
            && (self.read_ram(self.pc) == 0x00 && self.read_ram(self.pc + 1) == 0x00)
        {
            println!("Program Finished");
            return true;
        }

        false
    }

    pub fn execute_program(&mut self) {
        while !self.is_complete() {
            self.clock();
        }
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::*;
    use crate::bus::Bus;
    use crate::cpu_6502::CPU;

    #[test]
    /* it should take the program as a string representing u8 seperated by spaces and return a vector of u8 */
    fn parse_program_to_hex_test() -> Result<(), String> {
        /*
            *=$8000
            LDX #10
            STX $0000
            LDX #3
            STX $0001
            LDY $0000
            LDA #0
            CLC
            loop
            ADC $0001
            DEY
            BNE loop
            STA $0002
            NOP
            NOP
            NOP
        */
        let final_program: Vec<u8> = vec![
            0xA2, 0x0A, 0x8E, 0x00, 0x00, 0xA2, 0x03, 0x8E, 0x01, 0x00, 0xAC, 0x00, 0x00, 0xA9,
            0x00, 0x18, 0x6D, 0x01, 0x00, 0x88, 0xD0, 0xFA, 0x8D, 0x02, 0x00, 0xEA, 0xEA, 0xEA,
        ];
        let raw_program = String::from(
            "A2 0A 8E 00 00 A2 03 8E 01 00 AC 00 00 A9 00 18 6D 01 00 88 D0 FA 8D 02 00 EA EA EA",
        );
        let parsed_program = parse_program_to_hex(raw_program);
        for (index, opcode) in parsed_program.iter().enumerate() {
            assert_eq!(*opcode, final_program[index]);
        }

        Ok(())
    }

    #[test]
    /* it should take a program as a string and load it to the cpu and reset it to be ready to execute the program */
    fn load_program_test() -> Result<(), String> {
        let bus_with_ram = Bus::init();
        let mut cpu = CPU::init(bus_with_ram);
        let raw_program = String::from(
            "A2 0A 8E 00 00 A2 03 8E 01 00 AC 00 00 A9 00 18 6D 01 00 88 D0 FA 8D 02 00 EA EA EA",
        );
        let final_program: Vec<u8> = vec![
            0xA2, 0x0A, 0x8E, 0x00, 0x00, 0xA2, 0x03, 0x8E, 0x01, 0x00, 0xAC, 0x00, 0x00, 0xA9,
            0x00, 0x18, 0x6D, 0x01, 0x00, 0x88, 0xD0, 0xFA, 0x8D, 0x02, 0x00, 0xEA, 0xEA, 0xEA,
        ];
        let mut n_offset: u16 = 0x8000;

        cpu.load_program(raw_program);

        for program_elmt in final_program {
            assert_eq!(program_elmt, cpu.read_ram(n_offset));
            n_offset += 1;
        }

        Ok(())
    }

    #[test]
    /* it should return false if the program execution is not over */
    fn complete_program_not_over() -> Result<(), String> {
        // before: init a Bus and a CPU
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // program with one instruction:
        // *=$8000
        // LDX #10
        let program = String::from("A2 0A");
        cpu.load_program(program);

        // the reset take 8 cycle, execute clock to simulate the 8 cycles execution
        for _i in 0..8 {
            cpu.clock();
        }
        // check reset cycles are executed
        assert_eq!(cpu.cycles, 0);

        let is_complete = cpu.is_complete();

        assert_eq!(is_complete, false);

        Ok(())
    }

    #[test]
    /* it should return true if the program execution is over */
    fn complete_program_over() -> Result<(), String> {
        // before: init a Bus and a CPU
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // program with one instruction:
        // *=$8000
        // LDX #10
        let program = String::from("A2 0A");
        cpu.load_program(program);

        // the reset take 8 cycle, execute clock to simulate the 8 cycles execution
        for _i in 0..8 {
            cpu.clock();
        }
        // check reset cycles are executed
        assert_eq!(cpu.cycles, 0);

        let mut is_complete = cpu.is_complete();

        assert_eq!(is_complete, false);

        // execute the program
        // execute LDX on 2 cycles
        cpu.clock();
        cpu.clock();

        is_complete = cpu.is_complete();
        assert_eq!(is_complete, true);

        Ok(())
    }

    #[test]
    /* it should execute the program */
    fn execute_program() -> Result<(), String> {
        // before: init a Bus and a CPU
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // program with one instruction:
        // *=$8000
        // LDX #10
        let program = String::from("A2 0A");
        cpu.load_program(program);

        cpu.execute_program();

        assert_eq!(cpu.is_complete(), true);
        assert_eq!(cpu.x, 0x0A);
        assert_eq!(cpu.cycles, 0);

        Ok(())
    }

    // ### describe: Integration Tests

    // ## describe: Registers

    // it should multiply 10 by 3
    #[test]
    fn test_program_multiply_10_by_3() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
            LDX #10
            STX $0000
            LDX #3
            STX $0001
            LDY $0000
            LDA #0
            CLC
            loop
            ADC $0001
            DEY
            BNE loop
            STA $0002
            NOP
            NOP
            NOP
        */
        let raw_program = String::from(
            "A2 0A 8E 00 00 A2 03 8E 01 00 AC 00 00 A9 00 18 6D 01 00 88 D0 FA 8D 02 00 EA EA EA",
        );
        cpu.load_program(raw_program);
        cpu.execute_program();

        // the program multiply 10 by 3 and store the result to a
        assert_eq!(cpu.a, 0x1E);

        Ok(())
    }

    // it should add the value of the X register to the value of the A register
    #[test]
    fn test_program_add_registers_a_x() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
            LDA #$c0
            TAX
            INX
            ADC #$c4
            BRK
        */
        let raw_program = String::from("a9 c0 aa e8 69 c4 00");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.a, 0x84);
        assert_eq!(cpu.x, 0xC1);
        assert_eq!(cpu.pc, 0x8006);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0x01);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0x01);

        Ok(())
    }

    // ## describe: Branching

    // it should decrement the value in register X with a function (branching BNE - branch on not equal)
    #[test]
    fn test_program_branching_bne_decrement_function() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
              LDX #$08
            decrement:
              DEX
              STX $0200
              CPX #$03
              BNE decrement
              STX $0201
              BRK
        */
        let raw_program = String::from("a2 08 ca 8e 00 02 e0 03 d0 f8 8e 01 02 00");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.x, 0x03);
        assert_eq!(cpu.pc, 0x800D);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0x01);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0x01);

        Ok(())
    }

    // ## describe: Addressing Modes

    // it should use relative addresing mode to make some branching
    #[test]
    fn test_program_addressing_relative() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
              LDA #$02
              CMP #$01
              BNE notequal
              STA $22
            notequal:
              BRK
        */
        let raw_program = String::from("a9 02 c9 01 d0 02 85 22 00");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.a, 0x02);
        assert_eq!(cpu.pc, 0x8008);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0x01);

        Ok(())
    }

    // it should use indirect addresing mode (pointer) to make a jump
    #[test]
    fn test_program_addressing_indirect() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
              LDA #$02
              CMP #$01
              BNE notequal
              STA $22
            notequal:
              BRK
        */
        let raw_program = String::from("a9 01 85 f0 a9 cc 85 f1 6c f0 00");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.a, 0xCC);
        assert_eq!(cpu.pc, 0xCC01);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0x01);

        Ok(())
    }

    // it should use indexed indirect addresing mode (pointer). it is a mix between indirect and zero page x
    #[test]
    fn test_program_addressing_indexed_indirect() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
            LDX #$01
            LDA #$05
            STA $01
            LDA #$07
            STA $02
            LDY #$0a
            STY $0705
            LDA ($00,X)
        */
        let raw_program = String::from("a2 01 a9 05 85 01 a9 07 85 02 a0 0a 8c 05 07 a1 00");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.a, 0x0A);
        assert_eq!(cpu.x, 0x01);
        assert_eq!(cpu.y, 0x0A);
        assert_eq!(cpu.pc, 0x8011);

        Ok(())
    }

    // it should use indirect indexed addresing mode (pointer).
    #[test]
    fn test_program_addressing_indirect_indexed() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
            LDY #$01
            LDA #$03
            STA $01
            LDA #$07
            STA $02
            LDX #$0a
            STX $0704
            LDA ($01),Y
        */
        let raw_program = String::from("a0 01 a9 03 85 01 a9 07 85 02 a2 0a 8e 04 07 b1 01");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.a, 0x0A);
        assert_eq!(cpu.x, 0x0A);
        assert_eq!(cpu.y, 0x01);
        assert_eq!(cpu.pc, 0x8011);

        Ok(())
    }

    // ## describe: Stack

    // it should use the stack instructions PHA / PLA
    #[test]
    fn test_program_stack() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
              LDX #$00
              LDY #$00
            firstloop:
              TXA
              STA $0200,Y
              PHA
              INX
              INY
              CPY #$10
              BNE firstloop ;loop until Y is $10
            secondloop:
              PLA
              STA $0200,Y
              INY
              CPY #$20      ;loop until Y is $20
              BNE secondloop
        */
        let raw_program =
            String::from("a2 00 a0 00 8a 99 00 02 48 e8 c8 c0 10 d0 f5 68 99 00 02 c8 c0 20 d0 f7");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.x, 0x10);
        assert_eq!(cpu.y, 0x20);
        assert_eq!(cpu.pc, 0x8018);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0x01);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0x01);

        Ok(())
    }

    // ## describe: Jump

    // it should jump (JMP)
    #[test]
    fn test_program_jump_jmp() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
              LDA #$03
              JMP there
              BRK
              BRK
              BRK
            there:
              STA $0200
        */
        let raw_program = String::from("a9 03 4c 08 80 00 00 00 8d 00 02");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.a, 0x03);
        assert_eq!(cpu.pc, 0x800B);

        Ok(())
    }

    // it should jump to subroutine (JSR/RTS)
    #[test]
    fn test_program_jump_subroutine() -> Result<(), String> {
        // init the bus and the RAM
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        /*
            *=$8000
              JSR init
              JSR loop
              JSR end

            init:
              LDX #$00
              RTS

            loop:
              INX
              CPX #$05
              BNE loop
              RTS

            end:
              BRK
        */
        let raw_program = String::from("20 09 80 20 0c 80 20 12 80 a2 00 60 e8 e0 05 d0 fb 60 00");
        cpu.load_program(raw_program);
        cpu.execute_program();

        assert_eq!(cpu.x, 0x05);
        assert_eq!(cpu.pc, 0x8012);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0x01);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0x01);

        Ok(())
    }
}
