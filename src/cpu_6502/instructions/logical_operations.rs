use super::CPU;
use super::FLAGS6502;

// implement logical opeartions
impl CPU {
    // logical AND
    pub fn and(&mut self) -> u8 {
        self.fetch();
        self.a &= self.fetched;
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        1
    }

    // logical Exclusive OR
    pub fn eor(&mut self) -> u8 {
        self.fetch();
        self.a ^= self.fetched;
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        1
    }

    // logical OR
    pub fn ora(&mut self) -> u8 {
        self.fetch();
        self.a |= self.fetched;
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        1
    }

    // bit test
    pub fn bit(&mut self) -> u8 {
        self.fetch();
        let tmp = self.a & self.fetched;
        self.set_flag(FLAGS6502::Z, tmp == 0x00);
        self.set_flag(FLAGS6502::N, (self.fetched & (1 << 7)) != 0);
        self.set_flag(FLAGS6502::V, (self.fetched & (1 << 6)) != 0);

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should apply a logical AND between A register and the fetched data
    fn test_and_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xA5, [0xA0, 0x00]);
        cpu.a = 0x04;
        cpu.write_ram(0x00A0, 0x05);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.lda();

        // 0x04 & 0x05 = 0x04
        assert_eq!(cpu.a, 0x05);

        Ok(())
    }

    #[test]
    // it should set the Z (zero) flag, if the logical AND is equal to zero
    fn test_and_instruction_set_z_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x25, [0xA0, 0x00]);
        cpu.a = 0x04;
        cpu.write_ram(0x00A0, 0x02);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.and();

        // 0x04 & 0x05 = 0x04
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0x01);
        Ok(())
    }

    #[test]
    // it should set the N (negative) flag
    fn test_and_instruction_set_n_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x25, [0xA0, 0x00]);
        cpu.a = 0x80;
        cpu.write_ram(0x00A0, 0x80);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.and();

        // 0x04 & 0x05 = 0x04
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0x01);
        Ok(())
    }

    #[test]
    // it should apply a logical EOR between A register and the fetched data and set the negative flag
    fn test_eor_instruction_negative_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x45, [0xA0, 0x00]);
        cpu.a = 0xC1;
        cpu.write_ram(0x00A0, 0x49);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.eor();

        // 0xC1 ^ 0x49 = 0x88
        assert_eq!(cpu.a, 0x88);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should apply a logical EOR between A register and the fetched data and set the zero flag
    fn test_eor_instruction_zero_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x45, [0xA0, 0x00]);
        cpu.a = 0xFF;
        cpu.write_ram(0x00A0, 0xFF);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.eor();

        // 0xFF ^ 0xFF = 0x00
        assert_eq!(cpu.a, 0x00);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 1);

        Ok(())
    }

    #[test]
    // it should apply a logical OR between A register and the fetched data and set the negative flag
    fn test_ora_instruction_negative_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x75, [0xA0, 0x00]);
        cpu.a = 0xC1;
        cpu.write_ram(0x00A0, 0xC9);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.ora();

        // 0xC1 | 0xC9 = 0xC9
        assert_eq!(cpu.a, 0xC9);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should apply a logical OR between A register and the fetched data and set the zero flag
    fn test_ora_instruction_zero_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x75, [0xA0, 0x00]);
        cpu.a = 0x00;
        cpu.write_ram(0x00A0, 0x00);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.ora();

        // 0x00 | 0x00 = 0x00
        assert_eq!(cpu.a, 0x00);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 1);

        Ok(())
    }

    #[test]
    // it should make a AND between A and fetched (not saved), set or clear the zero flag and copy 7 and 6 byte as negative and overflow flag
    fn test_bits_instruction_zero_negative_overflow_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x24, [0xA0, 0x00]);
        cpu.set_flag(FLAGS6502::Z, true);
        cpu.a = 0xC1;
        cpu.write_ram(0x00A0, 0xC9);

        // execute the data fetching (addressing mode)
        cpu.zp0();

        cpu.bit();

        // 0x00 | 0x00 = 0x00
        assert_eq!(cpu.a, 0xC1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 1);

        Ok(())
    }
}
