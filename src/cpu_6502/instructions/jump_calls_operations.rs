use super::CPU;
use super::STACK_ADDR;

// implement jump and calls operations
impl CPU {
    // jump to an address
    pub fn jmp(&mut self) -> u8 {
        self.pc = self.addr_abs;

        0
    }

    // jump to a subroutine
    pub fn jsr(&mut self) -> u8 {
        self.pc -= 1;

        self.write_ram(
            STACK_ADDR + self.stk_p as u16,
            ((self.pc >> 8) & 0x00FF) as u8,
        );
        self.stk_p -= 1;
        self.write_ram(STACK_ADDR + self.stk_p as u16, (self.pc & 0x00FF) as u8);
        self.stk_p -= 1;

        self.pc = self.addr_abs;

        0
    }

    // return from a subroutine
    pub fn rts(&mut self) -> u8 {
        self.stk_p += 1;
        self.pc = self.read_ram(STACK_ADDR + self.stk_p as u16) as u16;
        self.stk_p += 1;
        self.pc |= (self.read_ram(STACK_ADDR + self.stk_p as u16) as u16) << 8;

        self.pc += 1;

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should set the pc to the address from the operand
    fn test_jmp_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x66, [0xA0, 0x00]);

        cpu.abs();
        cpu.jmp();

        assert_eq!(cpu.pc, 0xA0);

        Ok(())
    }

    #[test]
    // it should push the address of the return point on the stack and then set the pc to the target memory
    fn test_jsr_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x20, [0xA0, 0x00]);

        cpu.abs();
        cpu.jsr();

        // check if pc - 1 is on the stack
        // it is 8002
        assert_eq!(cpu.read_ram(0x01FD), 0x80);
        assert_eq!(cpu.read_ram(0x01FC), 0x02);

        // check stack pointer was updated
        assert_eq!(cpu.stk_p, 0xFB);

        // check if pc was updated
        assert_eq!(cpu.pc, 0xA0);

        Ok(())
    }

    #[test]
    // it should pop the address of the return point from the stack and then set the pc to the target
    fn test_rts_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x20, [0xA0, 0x00]);

        cpu.abs();
        cpu.jsr();

        cpu.imp();
        cpu.rts();

        // check if pc - 1 is on the stack
        // it is 8002
        assert_eq!(cpu.read_ram(0x01FD), 0x80);
        assert_eq!(cpu.read_ram(0x01FC), 0x02);

        // check if pc was updated and incremented
        assert_eq!(cpu.pc, 0x8003);
        // check the stack pointer was reset
        assert_eq!(cpu.stk_p, 0xFD);

        Ok(())
    }
}
