use super::CPU;
use super::FLAGS6502;

// implement increment / decrement operations
impl CPU {
    // increment the fetched data
    pub fn inc(&mut self) -> u8 {
        self.fetch();
        let tmp = self.fetched + 1;
        self.write_ram(self.addr_abs, tmp);
        self.set_flag(FLAGS6502::Z, tmp == 0);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);

        0
    }

    // increment the x register
    pub fn inx(&mut self) -> u8 {
        self.x += 1;
        self.set_flag(FLAGS6502::Z, self.x == 0x00);
        self.set_flag(FLAGS6502::N, (self.x & 0x80) != 0);

        0
    }

    // increment the y register
    pub fn iny(&mut self) -> u8 {
        self.y += 1;
        self.set_flag(FLAGS6502::Z, self.y == 0x00);
        self.set_flag(FLAGS6502::N, (self.y & 0x80) != 0);

        0
    }

    // decrement the fetched data
    pub fn dec(&mut self) -> u8 {
        self.fetch();
        let tmp = self.fetched - 1;
        self.write_ram(self.addr_abs, tmp);
        self.set_flag(FLAGS6502::Z, tmp == 0);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);

        0
    }

    // decrement the x register
    pub fn dex(&mut self) -> u8 {
        self.x -= 1;
        self.set_flag(FLAGS6502::Z, self.x == 0x00);
        self.set_flag(FLAGS6502::N, (self.x & 0x80) != 0);

        0
    }

    // decrement the y register
    pub fn dey(&mut self) -> u8 {
        self.y -= 1;
        self.set_flag(FLAGS6502::Z, self.y == 0x00);
        self.set_flag(FLAGS6502::N, (self.y & 0x80) != 0);

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should increment fetched data and set N,Z flags
    fn test_inc_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xE6, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.inc();

        assert_eq!(cpu.read_ram(0x00A0), 0x88);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should increment the x register and set N,Z flags
    fn test_inx_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xE8, [0xA0, 0x00]);
        cpu.x = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.inx();

        assert_eq!(cpu.x, 0x88);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should increment the y register and set N,Z flags
    fn test_iny_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xC8, [0xA0, 0x00]);
        cpu.y = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.iny();

        assert_eq!(cpu.y, 0x88);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should decrement fetched data and set N,Z flags
    fn test_dec_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xE6, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.dec();

        assert_eq!(cpu.read_ram(0x00A0), 0x86);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should decrement the x register and set N,Z flags
    fn test_dex_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xCA, [0xA0, 0x00]);
        cpu.x = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.dex();

        assert_eq!(cpu.x, 0x86);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should decrement the y register and set N,Z flags
    fn test_dey_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xCA, [0xA0, 0x00]);
        cpu.y = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.dey();

        assert_eq!(cpu.y, 0x86);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }
}
