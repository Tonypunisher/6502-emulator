use super::CPU;
use super::FLAGS6502;
use super::STACK_ADDR;

pub struct Instruction {
    pub name: String,
    pub execute: fn(cpu: &mut CPU) -> u8,
    pub addrmode_name: String,
    pub addrmode: fn(cpu: &mut CPU) -> u8,
    pub cycles: u8,
}

mod arithmetic_operations;
mod branches_operations;
mod increment_decrement_operations;
mod jump_calls_operations;
mod load_store_operations;
mod logical_operations;
mod registers_transfert_operations;
mod shift_operations;
mod stack_operations;
mod status_flag_changes_operations;
mod system_operations;

fn noop_execute(_cpu: &mut CPU) -> u8 {
    println!("Illegal opcode execution");

    0
}

fn noop_addr_mode(_cpu: &mut CPU) -> u8 {
    0x00
}

impl CPU {
    pub fn get_instruction(&self, opcode: u8) -> Instruction {
        match opcode {
            // load / store operations
            // LDA
            0xA9 => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xA5 => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xB5 => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0xAD => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0xBD => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0xB9 => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0xA1 => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0xB1 => Instruction {
                name: String::from("LDA"),
                execute: CPU::lda,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // LDX
            0xA2 => Instruction {
                name: String::from("LDX"),
                execute: CPU::ldx,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xA6 => Instruction {
                name: String::from("LDX"),
                execute: CPU::ldx,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xB6 => Instruction {
                name: String::from("LDX"),
                execute: CPU::ldx,
                addrmode_name: String::from("ZPY"),
                addrmode: CPU::zpy,
                cycles: 4,
            },
            0xAE => Instruction {
                name: String::from("LDX"),
                execute: CPU::ldx,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0xBE => Instruction {
                name: String::from("LDX"),
                execute: CPU::ldx,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            // LDY
            0xA0 => Instruction {
                name: String::from("LDY"),
                execute: CPU::ldy,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xA4 => Instruction {
                name: String::from("LDY"),
                execute: CPU::ldy,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xB4 => Instruction {
                name: String::from("LDY"),
                execute: CPU::ldy,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0xAC => Instruction {
                name: String::from("LDY"),
                execute: CPU::ldy,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0xBC => Instruction {
                name: String::from("LDY"),
                execute: CPU::ldy,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            // STA
            0x85 => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x95 => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0x8D => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0x9D => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 5,
            },
            0x99 => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 5,
            },
            0x81 => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0x91 => Instruction {
                name: String::from("STA"),
                execute: CPU::sta,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 6,
            },
            // STX
            0x86 => Instruction {
                name: String::from("STX"),
                execute: CPU::stx,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x96 => Instruction {
                name: String::from("STX"),
                execute: CPU::stx,
                addrmode_name: String::from("ZPY"),
                addrmode: CPU::zpy,
                cycles: 4,
            },
            0x8E => Instruction {
                name: String::from("STX"),
                execute: CPU::stx,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            // STY
            0x84 => Instruction {
                name: String::from("STY"),
                execute: CPU::sty,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x94 => Instruction {
                name: String::from("STY"),
                execute: CPU::sty,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0x8C => Instruction {
                name: String::from("STY"),
                execute: CPU::sty,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            // register transfert operations
            // TAX
            0xAA => Instruction {
                name: String::from("TAX"),
                execute: CPU::tax,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // TAY
            0xA8 => Instruction {
                name: String::from("TAY"),
                execute: CPU::tay,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // TXA
            0x8A => Instruction {
                name: String::from("TXA"),
                execute: CPU::txa,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // TYA
            0x98 => Instruction {
                name: String::from("TYA"),
                execute: CPU::tya,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // stack operations
            // TSX
            0xBA => Instruction {
                name: String::from("TSX"),
                execute: CPU::tsx,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // TXS
            0x9A => Instruction {
                name: String::from("TXS"),
                execute: CPU::txs,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // PHA
            0x48 => Instruction {
                name: String::from("PHA"),
                execute: CPU::pha,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 3,
            },
            // PHP
            0x08 => Instruction {
                name: String::from("PHP"),
                execute: CPU::php,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 3,
            },
            // PLA
            0x68 => Instruction {
                name: String::from("PLA"),
                execute: CPU::pla,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 4,
            },
            // PLP
            0x28 => Instruction {
                name: String::from("PLP"),
                execute: CPU::plp,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 4,
            },
            // logical operations
            // AND
            0x29 => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0x25 => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x35 => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0x2D => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0x3D => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0x39 => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0x21 => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0x31 => Instruction {
                name: String::from("AND"),
                execute: CPU::and,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // EOR
            0x49 => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0x45 => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x55 => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0x4D => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0x5D => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0x59 => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0x41 => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0x51 => Instruction {
                name: String::from("EOR"),
                execute: CPU::eor,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // ORA
            0x09 => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0x05 => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x15 => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0x0D => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0x1D => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0x19 => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0x01 => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0x11 => Instruction {
                name: String::from("ORA"),
                execute: CPU::ora,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // BIT
            0x24 => Instruction {
                name: String::from("BIT"),
                execute: CPU::bit,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x2C => Instruction {
                name: String::from("BIT"),
                execute: CPU::bit,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            // arithmetic operations
            // ADC
            0x69 => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0x65 => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0x75 => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0x6D => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0x7D => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0x79 => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0x61 => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0x71 => Instruction {
                name: String::from("ADC"),
                execute: CPU::adc,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // SBC
            0xE9 => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xE5 => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xF5 => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0xED => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0xFD => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0xF9 => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0xE1 => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0xF1 => Instruction {
                name: String::from("SBC"),
                execute: CPU::sbc,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // CMP
            0xC9 => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xC5 => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xD5 => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 4,
            },
            0xCD => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            0xDD => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 4,
            },
            0xD9 => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("ABY"),
                addrmode: CPU::aby,
                cycles: 4,
            },
            0xC1 => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("IZX"),
                addrmode: CPU::izx,
                cycles: 6,
            },
            0xD1 => Instruction {
                name: String::from("CMP"),
                execute: CPU::cmp,
                addrmode_name: String::from("IZY"),
                addrmode: CPU::izy,
                cycles: 5,
            },
            // CPX
            0xE0 => Instruction {
                name: String::from("CPX"),
                execute: CPU::cpx,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xE4 => Instruction {
                name: String::from("CPX"),
                execute: CPU::cpx,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xEC => Instruction {
                name: String::from("CPX"),
                execute: CPU::cpx,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            // CPY
            0xC0 => Instruction {
                name: String::from("CPY"),
                execute: CPU::cpy,
                addrmode_name: String::from("IMM"),
                addrmode: CPU::imm,
                cycles: 2,
            },
            0xC4 => Instruction {
                name: String::from("CPY"),
                execute: CPU::cpy,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 3,
            },
            0xCC => Instruction {
                name: String::from("CPY"),
                execute: CPU::cpy,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 4,
            },
            // increment and decrement operation
            // INC
            0xE6 => Instruction {
                name: String::from("INC"),
                execute: CPU::inc,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 5,
            },
            0xF6 => Instruction {
                name: String::from("INC"),
                execute: CPU::inc,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 6,
            },
            0xEE => Instruction {
                name: String::from("INC"),
                execute: CPU::inc,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            0xFE => Instruction {
                name: String::from("INC"),
                execute: CPU::inc,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 7,
            },
            // INX
            0xE8 => Instruction {
                name: String::from("INX"),
                execute: CPU::inx,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // INY
            0xC8 => Instruction {
                name: String::from("INY"),
                execute: CPU::iny,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // DEC
            0xC6 => Instruction {
                name: String::from("DEC"),
                execute: CPU::dec,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 5,
            },
            0xD6 => Instruction {
                name: String::from("DEC"),
                execute: CPU::dec,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 6,
            },
            0xCE => Instruction {
                name: String::from("DEC"),
                execute: CPU::dec,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            0xDE => Instruction {
                name: String::from("DEC"),
                execute: CPU::dec,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 7,
            },
            // DEX
            0xCA => Instruction {
                name: String::from("DEX"),
                execute: CPU::dex,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // DEY
            0x88 => Instruction {
                name: String::from("DEY"),
                execute: CPU::dey,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // shifts operations
            // ASL
            // TODO: check if addressing mode is OK
            0x0A => Instruction {
                name: String::from("ASL"),
                execute: CPU::asl,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            0x06 => Instruction {
                name: String::from("ASL"),
                execute: CPU::asl,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 5,
            },
            0x16 => Instruction {
                name: String::from("ASL"),
                execute: CPU::asl,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 6,
            },
            0x0E => Instruction {
                name: String::from("ASL"),
                execute: CPU::asl,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            0x1E => Instruction {
                name: String::from("ASL"),
                execute: CPU::asl,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 7,
            },
            // LSR
            0x4A => Instruction {
                name: String::from("LSR"),
                execute: CPU::lsr,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            0x46 => Instruction {
                name: String::from("LSR"),
                execute: CPU::lsr,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 5,
            },
            0x56 => Instruction {
                name: String::from("LSR"),
                execute: CPU::lsr,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 6,
            },
            0x4E => Instruction {
                name: String::from("LSR"),
                execute: CPU::lsr,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            0x5E => Instruction {
                name: String::from("LSR"),
                execute: CPU::lsr,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 7,
            },
            // ROL
            0x2A => Instruction {
                name: String::from("ROL"),
                execute: CPU::rol,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            0x26 => Instruction {
                name: String::from("ROL"),
                execute: CPU::rol,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 5,
            },
            0x36 => Instruction {
                name: String::from("ROL"),
                execute: CPU::rol,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 6,
            },
            0x2E => Instruction {
                name: String::from("ROL"),
                execute: CPU::rol,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            0x3E => Instruction {
                name: String::from("ROL"),
                execute: CPU::rol,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 7,
            },
            // ROR
            0x6A => Instruction {
                name: String::from("ROR"),
                execute: CPU::ror,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            0x66 => Instruction {
                name: String::from("ROR"),
                execute: CPU::ror,
                addrmode_name: String::from("ZP0"),
                addrmode: CPU::zp0,
                cycles: 5,
            },
            0x76 => Instruction {
                name: String::from("ROR"),
                execute: CPU::ror,
                addrmode_name: String::from("ZPX"),
                addrmode: CPU::zpx,
                cycles: 6,
            },
            0x6E => Instruction {
                name: String::from("ROR"),
                execute: CPU::ror,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            0x7E => Instruction {
                name: String::from("ROR"),
                execute: CPU::ror,
                addrmode_name: String::from("ABX"),
                addrmode: CPU::abx,
                cycles: 7,
            },
            // jump and call operations
            // JMP
            0x4C => Instruction {
                name: String::from("JMP"),
                execute: CPU::jmp,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 3,
            },
            0x6C => Instruction {
                name: String::from("JMP"),
                execute: CPU::jmp,
                addrmode_name: String::from("IND"),
                addrmode: CPU::ind,
                cycles: 5,
            },
            // JSR
            0x20 => Instruction {
                name: String::from("JSR"),
                execute: CPU::jsr,
                addrmode_name: String::from("ABS"),
                addrmode: CPU::abs,
                cycles: 6,
            },
            // RTS
            0x60 => Instruction {
                name: String::from("RTS"),
                execute: CPU::rts,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 6,
            },
            // branches operations
            // BCC
            0x90 => Instruction {
                name: String::from("BCC"),
                execute: CPU::bcc,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BCS
            0xB0 => Instruction {
                name: String::from("BCS"),
                execute: CPU::bcs,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BEQ
            0xF0 => Instruction {
                name: String::from("BEQ"),
                execute: CPU::beq,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BMI
            0x30 => Instruction {
                name: String::from("BMI"),
                execute: CPU::bmi,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BNE
            0xD0 => Instruction {
                name: String::from("BNE"),
                execute: CPU::bne,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BPL
            0x10 => Instruction {
                name: String::from("BPL"),
                execute: CPU::bpl,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BVC
            0x50 => Instruction {
                name: String::from("BVC"),
                execute: CPU::bvc,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // BVS
            0x70 => Instruction {
                name: String::from("BVS"),
                execute: CPU::bvs,
                addrmode_name: String::from("REL"),
                addrmode: CPU::rel,
                cycles: 2,
            },
            // status flag changes operations
            // CLC
            0x18 => Instruction {
                name: String::from("CLC"),
                execute: CPU::clc,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // CLD
            0xD8 => Instruction {
                name: String::from("CLD"),
                execute: CPU::cld,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // CLI
            0x58 => Instruction {
                name: String::from("CLI"),
                execute: CPU::cli,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // CLV
            0xB8 => Instruction {
                name: String::from("CLV"),
                execute: CPU::clv,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // SEC
            0x38 => Instruction {
                name: String::from("SEC"),
                execute: CPU::sec,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // SED
            0xF8 => Instruction {
                name: String::from("SED"),
                execute: CPU::sed,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // SEI
            0x78 => Instruction {
                name: String::from("SEI"),
                execute: CPU::sei,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // system functions
            // BRK
            0x00 => Instruction {
                name: String::from("BRK"),
                execute: CPU::brk,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 7,
            },
            // RTI
            0x40 => Instruction {
                name: String::from("RTI"),
                execute: CPU::rti,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            // NOP
            0xEA => Instruction {
                name: String::from("NOP"),
                execute: CPU::nop,
                addrmode_name: String::from("IMP"),
                addrmode: CPU::imp,
                cycles: 2,
            },
            _ => Instruction {
                name: String::from("illegal"),
                execute: noop_execute,
                addrmode_name: String::from("IMP"),
                addrmode: noop_addr_mode,
                cycles: 1,
            },
        }
    }

    fn fetch(&mut self) -> u8 {
        // fetch the data at the address (because the addressing mode function already retrieve it)
        // if the dressing modes is IMP (IMPlied) the fetched variable is already setted.
        if self.get_instruction(self.opcode).addrmode_name != "IMP" {
            self.fetched = self.read_ram(self.addr_abs);
        }

        self.fetched
    }
}
