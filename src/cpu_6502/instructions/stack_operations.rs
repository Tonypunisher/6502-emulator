use super::CPU;
use super::FLAGS6502;
use super::STACK_ADDR;

// implement stack opeartion
impl CPU {
    // push a register content on the stack
    pub fn pha(&mut self) -> u8 {
        self.write_ram(STACK_ADDR + self.stk_p as u16, self.a);
        self.stk_p -= 1;

        0
    }

    // pull stack content to a register
    pub fn pla(&mut self) -> u8 {
        self.stk_p += 1;
        self.a = self.read_ram(STACK_ADDR + self.stk_p as u16);
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        0
    }

    // transfert x register content to the stack pointer
    pub fn tsx(&mut self) -> u8 {
        self.x = self.stk_p;
        self.set_flag(FLAGS6502::Z, self.x == 0x00);
        self.set_flag(FLAGS6502::N, (self.x & 0x80) != 0);

        0
    }

    // transfert the stack pointer to x register
    pub fn txs(&mut self) -> u8 {
        self.stk_p = self.x;

        0
    }

    // pull status register on the stack
    pub fn php(&mut self) -> u8 {
        self.write_ram(STACK_ADDR + self.stk_p as u16, self.status);
        self.stk_p -= 1;

        0
    }

    // pull the content from the stack in status register
    pub fn plp(&mut self) -> u8 {
        self.stk_p += 1;
        self.status = self.read_ram(STACK_ADDR + self.stk_p as u16);

        0
    }
}

#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should push the accumulator value to the stack
    fn test_pha_instruction_push_accumulator_to_stack() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x68, [0x00, 0x00]);
        cpu.a = 0xAA;

        cpu.imp();

        cpu.pha();

        // note the based stack address is 0x0100
        // and we add it the value of the stack pointer (0xFD)
        assert_eq!(cpu.read_ram(0x1FD), 0xAA);
        assert_eq!(cpu.stk_p, 0xFC);
        Ok(())
    }

    #[test]
    // it should pop the accumulator value from the stack and set the zero flag
    fn test_pla_instruction_pop_accumulator_from_stack_zero_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x68, [0x00, 0x00]);
        cpu.a = 0x00;
        // push a value on the stack
        cpu.imp();
        cpu.pha();
        // change a register value to be sure to pop the correct value after
        cpu.a = 0x0A;

        // pop a value from the stack
        cpu.imp();

        cpu.pla();

        assert_eq!(cpu.a, 0x00);
        assert_eq!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 1);
        Ok(())
    }

    #[test]
    // it should pop the accumulator value from the stack and set the negative flag
    fn test_pla_instruction_pop_accumulator_from_stack_negative_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x68, [0x00, 0x00]);
        cpu.a = 0xF0;
        // push a value on the stack
        cpu.imp();
        cpu.pha();
        // change a register value to be sure to pop the correct value after
        cpu.a = 0x0A;

        // pop a value from the stack
        cpu.imp();

        cpu.pla();

        assert_eq!(cpu.a, 0xF0);
        assert_eq!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);
        Ok(())
    }

    #[test]
    // it should transfert the stack pointer to the x register and set N and Z flags
    fn test_tsx_instruction_copy_stk_p_on_x_register() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xBA, [0x00, 0x00]);
        cpu.set_flag(FLAGS6502::Z, true);
        cpu.x = 0xF0;

        cpu.imp();
        cpu.tsx();

        assert_ne!(cpu.x, 0xF0);
        assert_eq!(cpu.x, 0xFD);
        assert_eq!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        Ok(())
    }

    #[test]
    // it should transfert the x register content into the stack
    fn test_txs_instruction_copy_x_register_to_stack_pointer() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x9A, [0x00, 0x00]);
        cpu.x = 0xF0;

        cpu.imp();
        cpu.txs();

        assert_eq!(cpu.x, 0xF0);
        assert_ne!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.stk_p, 0xF0);
        Ok(())
    }

    #[test]
    // it should push the status flag on the stack
    fn test_php_push_status_on_stack() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x08, [0x00, 0x00]);
        // init status flag to 0xE1
        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::V, true);
        cpu.set_flag(FLAGS6502::U, true);
        cpu.set_flag(FLAGS6502::C, true);

        cpu.imp();
        cpu.php();

        assert_eq!(cpu.read_ram(0x1FD), 0xE1);
        assert_eq!(cpu.stk_p, 0xFC);
        Ok(())
    }

    #[test]
    // it should pop the status flag from the stack and reset it.
    fn test_plp_push_status_on_stack() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x28, [0x00, 0x00]);
        // init status flag to 0xE1
        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::V, true);
        cpu.set_flag(FLAGS6502::U, true);
        cpu.set_flag(FLAGS6502::C, true);

        // push it on the stack
        cpu.imp();
        cpu.php();

        cpu.set_flag(FLAGS6502::N, false);
        cpu.set_flag(FLAGS6502::V, false);
        cpu.set_flag(FLAGS6502::U, false);
        cpu.set_flag(FLAGS6502::C, false);

        // pulll it from the stack
        cpu.imp();
        cpu.plp();

        assert_eq!(cpu.stk_p, 0xFD);
        assert_eq!(cpu.status, 0xE1);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::I), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::D), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::B), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::U), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);
        Ok(())
    }
}
