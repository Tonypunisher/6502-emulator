use super::CPU;
use super::FLAGS6502;

// implement register transfert operations
impl CPU {
    // transfert a to x
    pub fn tax(&mut self) -> u8 {
        self.x = self.a;
        self.set_flag(FLAGS6502::Z, self.x == 0x00);
        self.set_flag(FLAGS6502::N, (self.x & 0x80) != 0);

        0
    }

    // transfert a to y
    pub fn tay(&mut self) -> u8 {
        self.y = self.a;
        self.set_flag(FLAGS6502::Z, self.y == 0x00);
        self.set_flag(FLAGS6502::N, (self.y & 0x80) != 0);

        0
    }

    // transfert x to a
    pub fn txa(&mut self) -> u8 {
        self.a = self.x;
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        0
    }

    // transfert y to a
    pub fn tya(&mut self) -> u8 {
        self.a = self.y;
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        0
    }
}

#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should transfert accumulator (a) to x and set Z,N flags
    fn test_tax_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xAA, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.tax();

        assert_eq!(cpu.x, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should transfert accumulator (a) to y and set Z,N flags
    fn test_tay_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xA8, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.tay();

        assert_eq!(cpu.y, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should transfert x to accumulator (a) and set Z,N flags
    fn test_txa_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x8A, [0xA0, 0x00]);
        cpu.x = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.txa();

        assert_eq!(cpu.a, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should transfert y to accumulator (a) and set Z,N flags
    fn test_tya_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xA8, [0xA0, 0x00]);
        cpu.y = 0x87;

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.tya();

        assert_eq!(cpu.a, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }
}
