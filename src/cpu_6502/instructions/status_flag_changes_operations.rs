use super::CPU;
use super::FLAGS6502;

// implement status flags changes
impl CPU {
    // set C flag to 0
    pub fn clc(&mut self) -> u8 {
        self.set_flag(FLAGS6502::C, false);

        0
    }

    // set D flag to 0
    pub fn cld(&mut self) -> u8 {
        self.set_flag(FLAGS6502::D, false);

        0
    }

    // set I flag to 0
    pub fn cli(&mut self) -> u8 {
        self.set_flag(FLAGS6502::I, false);

        0
    }

    // set V flag to 0
    pub fn clv(&mut self) -> u8 {
        self.set_flag(FLAGS6502::V, false);

        0
    }

    // set C flag to 1
    pub fn sec(&mut self) -> u8 {
        self.set_flag(FLAGS6502::C, true);

        0
    }

    // set D flag to 1
    pub fn sed(&mut self) -> u8 {
        self.set_flag(FLAGS6502::D, true);

        0
    }

    // set I flag to 1
    pub fn sei(&mut self) -> u8 {
        self.set_flag(FLAGS6502::I, true);

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should clear the Carry flag
    fn test_clc_instruction_clear_carry_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x18, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::C, true);

        cpu.imp();

        cpu.clc();

        assert_eq!(cpu.get_flag(FLAGS6502::C), 0);
        Ok(())
    }

    #[test]
    // it should clear the Decimal flag
    fn test_cld_instruction_clear_decimal_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xD8, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::D, true);

        cpu.imp();

        cpu.cld();

        assert_eq!(cpu.get_flag(FLAGS6502::D), 0);
        Ok(())
    }

    #[test]
    // it should clear the Interrupt Disable flag
    fn test_cli_instruction_clear_interrupt_disable_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x58, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::I, true);

        cpu.imp();

        cpu.cli();

        assert_eq!(cpu.get_flag(FLAGS6502::I), 0);
        Ok(())
    }

    #[test]
    // it should clear the Overflow flag
    fn test_clv_instruction_clear_overflow_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xB8, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::V, true);

        cpu.imp();

        cpu.clv();

        assert_eq!(cpu.get_flag(FLAGS6502::V), 0);
        Ok(())
    }

    #[test]
    // it should set the Carry flag
    fn test_sec_instruction_set_carry_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x38, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::C, false);

        cpu.imp();

        cpu.sec();

        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        Ok(())
    }

    #[test]
    // it should set the Decimal flag
    fn test_sed_instruction_set_decimal_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xF8, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::D, false);

        cpu.imp();

        cpu.sed();

        assert_eq!(cpu.get_flag(FLAGS6502::D), 1);
        Ok(())
    }

    #[test]
    // it should set the Interrupt Disable flag
    fn test_sei_instruction_set_interrupt_disable_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x78, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::I, false);

        cpu.imp();

        cpu.sei();

        assert_eq!(cpu.get_flag(FLAGS6502::I), 1);
        Ok(())
    }
}
