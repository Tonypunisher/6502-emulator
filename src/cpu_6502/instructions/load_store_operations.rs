use super::CPU;
use super::FLAGS6502;

// implement load / store operations
impl CPU {
    // load accumulator
    pub fn lda(&mut self) -> u8 {
        self.fetch();
        self.a = self.fetched;
        self.set_flag(FLAGS6502::Z, self.a == 0x00);
        self.set_flag(FLAGS6502::N, (self.a & 0x80) != 0);

        1
    }

    // load x
    pub fn ldx(&mut self) -> u8 {
        self.fetch();
        self.x = self.fetched;
        self.set_flag(FLAGS6502::Z, self.x == 0x00);
        self.set_flag(FLAGS6502::N, (self.x & 0x80) != 0);

        1
    }

    // load y
    pub fn ldy(&mut self) -> u8 {
        self.fetch();
        self.y = self.fetched;
        self.set_flag(FLAGS6502::Z, self.y == 0x00);
        self.set_flag(FLAGS6502::N, (self.y & 0x80) != 0);

        1
    }

    // store a
    pub fn sta(&mut self) -> u8 {
        self.fetch();
        self.write_ram(self.addr_abs, self.a);

        0
    }

    // store x
    pub fn stx(&mut self) -> u8 {
        self.fetch();
        self.write_ram(self.addr_abs, self.x);

        0
    }

    // store y
    pub fn sty(&mut self) -> u8 {
        self.fetch();
        self.write_ram(self.addr_abs, self.y);

        0
    }
}

#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should fetch some data, set the a register and set Z,N flags
    fn test_lda_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xA5, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.lda();

        assert_eq!(cpu.a, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should fetch some data, set the x register and set N,Z flags
    fn test_ldx_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xA6, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.ldx();

        assert_eq!(cpu.x, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should fetch some data, set the y register and set N,Z flags
    fn test_ldy_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xA4, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.ldy();

        assert_eq!(cpu.y, 0x87);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);

        Ok(())
    }

    #[test]
    // it should store the data from memory to a
    fn test_sta_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x85, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.zp0();
        cpu.sta();

        assert_eq!(cpu.read_ram(0x00A0), 0x87);

        Ok(())
    }

    #[test]
    // it should store the data from memory to x
    fn test_stx_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x86, [0xA0, 0x00]);
        cpu.x = 0x87;

        cpu.zp0();
        cpu.stx();

        assert_eq!(cpu.read_ram(0x00A0), 0x87);

        Ok(())
    }

    #[test]
    // it should store the data from memory to y
    fn test_sty_instruction() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x84, [0xA0, 0x00]);
        cpu.y = 0x87;

        cpu.zp0();
        cpu.sty();

        assert_eq!(cpu.read_ram(0x00A0), 0x87);

        Ok(())
    }
}
