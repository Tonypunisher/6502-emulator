use super::CPU;
use super::FLAGS6502;

// implement arithmetic operations
impl CPU {
    // add a to fetched data and save the result to the a register
    pub fn adc(&mut self) -> u8 {
        self.fetch();
        let tmp = self.a as u16 + self.fetched as u16 + self.get_flag(FLAGS6502::C) as u16;
        self.set_flag(FLAGS6502::C, tmp > 255);
        self.set_flag(FLAGS6502::Z, (tmp & 0x00FF) == 0);
        self.set_flag(FLAGS6502::N, (tmp & 0x80) != 0);
        self.set_flag(
            FLAGS6502::V,
            ((!(self.a as u16 ^ self.fetched as u16) & (self.a as u16 ^ tmp)) & 0x0080) != 0,
        );
        self.a = (tmp & 0x00FF) as u8;

        1
    }

    // substract a to fetched data and save the result to the a register
    pub fn sbc(&mut self) -> u8 {
        self.fetch();

        let value = (self.fetched as u16) ^ 0x00FF;

        let tmp = self.a as u16 + value + self.get_flag(FLAGS6502::C) as u16;
        self.set_flag(FLAGS6502::C, tmp & 0xFF00 != 0);
        self.set_flag(FLAGS6502::Z, (tmp & 0x00FF) == 0);
        self.set_flag(FLAGS6502::N, tmp & 0x0080 != 0);
        self.set_flag(
            FLAGS6502::V,
            (tmp ^ self.a as u16) & (tmp ^ value) & 0x0080 != 0,
        );
        self.a = (tmp & 0x00FF) as u8;

        1
    }

    // compare a to fetched
    pub fn cmp(&mut self) -> u8 {
        self.fetch();
        let tmp = self.a.abs_diff(self.fetched);
        self.set_flag(FLAGS6502::C, self.a >= self.fetched);
        self.set_flag(FLAGS6502::Z, tmp == 0x0000);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);

        1
    }

    // compare x to fetched
    pub fn cpx(&mut self) -> u8 {
        self.fetch();
        let tmp = self.x.abs_diff(self.fetched);
        self.set_flag(FLAGS6502::C, self.x >= self.fetched);
        self.set_flag(FLAGS6502::Z, tmp == 0x0000);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);

        0
    }

    // compare y to fetched
    pub fn cpy(&mut self) -> u8 {
        self.fetch();
        let tmp = self.y.abs_diff(self.fetched);
        self.set_flag(FLAGS6502::C, self.y >= self.fetched);
        self.set_flag(FLAGS6502::Z, tmp == 0x0000);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    // here for addition, we test some hard cases. we test the overflow on signed or unsigned addition.
    #[test]
    // it should add 2 positive number, and set accumulator with positive number result
    fn test_adc_instruction_add_2_positive_result_positive() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x65, [0xA0, 0x00]);
        // we will add: A += fetched + C
        cpu.a = 0x10;
        cpu.write_ram(0x00A0, 0x10);

        cpu.zp0();

        cpu.adc();

        assert_eq!(cpu.a, 0x20);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 0);
        Ok(())
    }

    #[test]
    // it should add 2 positive number, and set accumulator with negative number result and set overflow and negative flag
    fn test_adc_instruction_add_2_positive_result_negative() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x65, [0xA0, 0x00]);
        // we will add: A += fetched + C
        cpu.a = 0x7F;
        cpu.write_ram(0x00A0, 0x04);

        cpu.zp0();

        cpu.adc();

        assert_eq!(cpu.a, 0x83);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 1);
        Ok(())
    }

    #[test]
    // it should add 2 negative number, and set accumulator with negative number result
    fn test_adc_instruction_add_2_negative_result_negative() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x65, [0xA0, 0x00]);
        // we will add: A += fetched + C
        cpu.a = 0xE4;
        cpu.write_ram(0x00A0, 0xE0);

        cpu.zp0();

        cpu.adc();

        assert_eq!(cpu.a, 0xC4);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 0);
        Ok(())
    }

    #[test]
    // it should add 2 negative number, and set accumulator with positive number result and set flags (overflow and carry)
    fn test_adc_instruction_add_2_negative_result_positive() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x65, [0xA0, 0x00]);
        // we will add: A += fetched + C
        cpu.a = 0x84;
        cpu.write_ram(0x00A0, 0x83);

        cpu.zp0();

        cpu.adc();

        assert_eq!(cpu.a, 0x07);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 1);
        Ok(())
    }

    // for substraction we use the addition code (we invert the fetched data and make an addition), so we don't repeat all tests here.

    #[test]
    // it should substract 2 number
    fn test_sbc_instruction_substract_2_numbers() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xE5, [0xA0, 0x00]);
        cpu.a = 0x80;
        cpu.write_ram(0x00A0, 0x05);

        cpu.zp0();

        cpu.sbc();

        assert_eq!(cpu.a, 0x7A);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::V), 1);
        Ok(())
    }

    #[test]
    // it should compare the content of a and fetched and set carry, zero and negative flags
    fn test_cmp_instruction_zero_negative_carry_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xC5, [0xA0, 0x00]);
        cpu.a = 0x80;
        cpu.set_flag(FLAGS6502::Z, true);
        cpu.set_flag(FLAGS6502::N, true);
        cpu.write_ram(0x00A0, 0x05);

        cpu.zp0();

        cpu.cmp();

        assert_eq!(cpu.a, 0x80);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        Ok(())
    }

    #[test]
    // it should compare the content of x and fetched and set carry, zero and negative flags
    fn test_cpx_instruction_zero_negative_carry_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xE4, [0xA0, 0x00]);
        cpu.x = 0x80;
        cpu.set_flag(FLAGS6502::Z, true);
        cpu.set_flag(FLAGS6502::N, true);
        cpu.write_ram(0x00A0, 0x05);

        cpu.zp0();

        cpu.cpx();

        assert_eq!(cpu.x, 0x80);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        Ok(())
    }

    #[test]
    // it should compare the content of x and fetched and set carry, zero and negative flags
    fn test_cpy_instruction_zero_negative_carry_flag() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xC4, [0xA0, 0x00]);
        cpu.y = 0x80;
        cpu.set_flag(FLAGS6502::Z, true);
        cpu.set_flag(FLAGS6502::N, true);
        cpu.write_ram(0x00A0, 0x05);

        cpu.zp0();

        cpu.cpy();

        assert_eq!(cpu.y, 0x80);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::C), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        Ok(())
    }
}
