use super::CPU;
use super::FLAGS6502;

// implement branches operations
impl CPU {
    // branches if carry flag not set
    pub fn bcc(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::C) == 0 {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }
        }

        0
    }

    // branches if carry flag set
    pub fn bcs(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::C) == 1 {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }
        }

        0
    }

    // branches if zero flag set
    pub fn beq(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::Z) == 1 {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }
        }

        0
    }

    // branches if negative flag set
    pub fn bmi(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::N) == 1 {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }
        }

        0
    }

    // branches if zero flag not set
    pub fn bne(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::Z) == 0 {
            self.cycles += 1;
            // pass values on u32 to be able to make the computation and repass to u16
            // this case append when the program is going backward
            self.addr_abs = (self.pc as u32 + self.addr_rel as u32) as u16;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }

        0
    }

    // branches if negative flag not set
    pub fn bpl(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::N) == 0 {
            self.cycles += 1;
            // pass values on u32 to be able to make the computation and repass to u16
            // this case append when the program is going backward
            self.addr_abs = (self.pc as u32 + self.addr_rel as u32) as u16;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }

        0
    }

    // branches if overflow flag not set
    pub fn bvc(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::V) == 0 {
            self.cycles += 1;
            // pass values on u32 to be able to make the computation and repass to u16
            // this case append when the program is going backward
            self.addr_abs = (self.pc as u32 + self.addr_rel as u32) as u16;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }

        0
    }

    // branches if overflow flag set
    pub fn bvs(&mut self) -> u8 {
        if self.get_flag(FLAGS6502::V) == 1 {
            self.cycles += 1;
            // pass values on u32 to be able to make the computation and repass to u16
            // this case append when the program is going backward
            self.addr_abs = (self.pc as u32 + self.addr_rel as u32) as u16;

            if (self.addr_abs & 0xFF00) != (self.pc & 0xFF00) {
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should execute bvs and do nothing if carry flag set
    fn test_bcc_instruction_carry_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x90, [0xA0, 0x00]);

        cpu.set_flag(FLAGS6502::C, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bvs();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bcc, offset pc with the addr_rel and save it as addr_abs
    fn test_bcc_instruction_carry_flag_not_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x90, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::C, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bcc();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bcc, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bcc_instruction_carry_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x90, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::C, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bcc();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute bcs and do nothing if carry flag is not set
    fn test_bcs_instruction_no_carry_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xB0, [0xA0, 0x00]);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bcs();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bcs, offset pc with the addr_rel and save it as addr_abs
    fn test_bcs_instruction_carry_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xB0, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::C, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bcs();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bcs, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bcs_instruction_carry_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xB0, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::C, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bcs();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute beq and do nothing if zero flag is not set
    fn test_beq_instruction_no_zero_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xF0, [0xA0, 0x00]);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.beq();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute beq, offset pc with the addr_rel and save it as addr_abs
    fn test_beq_instruction_zero_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xF0, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::Z, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.beq();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute beq, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_beq_instruction_zero_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xF0, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::Z, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.beq();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute bmi and do nothing if negative flag is not set
    fn test_bmi_instruction_no_negative_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x30, [0xA0, 0x00]);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bmi();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bmi, offset pc with the addr_rel and save it as addr_abs
    fn test_bmi_instruction_negative_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x30, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::N, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bmi();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bmi, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bmi_instruction_negative_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x30, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::N, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bmi();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute bne and do nothing if zero flag set
    fn test_bne_instruction_zero_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xD0, [0xA0, 0x00]);

        cpu.set_flag(FLAGS6502::Z, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bne();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bne, offset pc with the addr_rel and save it as addr_abs
    fn test_bne_instruction_zero_flag_not_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xD0, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::Z, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bne();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bne, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bne_instruction_zero_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xD0, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::Z, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bne();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute bpl and do nothing if negative flag set
    fn test_bpl_instruction_negative_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x10, [0xA0, 0x00]);

        cpu.set_flag(FLAGS6502::N, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bpl();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bpl, offset pc with the addr_rel and save it as addr_abs
    fn test_bpl_instruction_negative_flag_not_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x10, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::N, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bpl();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bpl, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bpl_instruction_negative_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x10, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::N, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bpl();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute bvc and do nothing if overflow flag set
    fn test_bvc_instruction_overflow_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x50, [0xA0, 0x00]);

        cpu.set_flag(FLAGS6502::V, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bvc();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bvc, offset pc with the addr_rel and save it as addr_abs
    fn test_bvc_instruction_overflow_flag_not_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x50, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::V, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bvc();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bvc, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bvc_instruction_overflow_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x50, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::V, false);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bvc();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }

    #[test]
    // it should execute bvs and do nothing if overflow flag not set
    fn test_bvs_instruction_overflow_flag_not_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x70, [0xA0, 0x00]);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bvs();

        // no changes here
        assert_eq!(cpu.addr_abs, 0x00);
        Ok(())
    }

    #[test]
    // it should execute bvs, offset pc with the addr_rel and save it as addr_abs
    fn test_bvs_instruction_overflow_flag_set() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x70, [0x04, 0x00]);

        cpu.set_flag(FLAGS6502::V, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        cpu.bvs();

        // offset 0x8002 with 0x04
        assert_eq!(cpu.addr_abs, 0x8006);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 9);
        Ok(())
    }

    #[test]
    // it should execute bvs, and add one addictionnal cycle if add_abs pass to the next memory page
    fn test_bvs_instruction_overflow_flag_next_memory_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x70, [0x05, 0x00]);

        cpu.set_flag(FLAGS6502::V, true);

        // execue the data fetching (addressing mode)
        cpu.rel();

        // change the address to be able to pass to the next memory page
        cpu.pc = 0x80FF;

        cpu.bvs();

        // offset 0x80FF with 0x05
        assert_eq!(cpu.addr_abs, 0x8104);
        // I just take the number of cycles from println
        assert_eq!(cpu.cycles, 10);
        Ok(())
    }
}
