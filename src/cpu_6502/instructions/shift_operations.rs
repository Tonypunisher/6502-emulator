use super::CPU;
use super::FLAGS6502;

// implement shifts operations
impl CPU {
    // shift left, save in the a register or in ram on the fetched address
    pub fn asl(&mut self) -> u8 {
        self.fetch();
        let tmp = self.fetched << 1;
        self.set_flag(FLAGS6502::C, (tmp as u16 & 0xFF00) > 0);
        self.set_flag(FLAGS6502::Z, tmp == 0x0000);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);
        if self.get_instruction(self.opcode).addrmode_name == *"IMP" {
            self.a = tmp;
        } else {
            self.write_ram(self.addr_abs, tmp);
        }

        0
    }

    // shift right, save in the a register or in ram on the fetched address
    pub fn lsr(&mut self) -> u8 {
        self.fetch();
        self.set_flag(FLAGS6502::C, (self.fetched & 0x0001) != 0);
        let tmp = self.fetched >> 1;
        self.set_flag(FLAGS6502::Z, tmp == 0x0000);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);
        if self.get_instruction(self.opcode).addrmode_name == *"IMP" {
            self.a = tmp;
        } else {
            self.write_ram(self.addr_abs, tmp);
        }

        0
    }

    // rotate left, save in the a register or in ram on the fetched address
    pub fn rol(&mut self) -> u8 {
        self.fetch();
        let tmp = self.fetched << 1 | self.get_flag(FLAGS6502::C);
        self.set_flag(FLAGS6502::C, (tmp as u16 & 0xFF00) > 0);
        self.set_flag(FLAGS6502::Z, tmp == 0x0000);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);
        if self.get_instruction(self.opcode).addrmode_name == *"IMP" {
            self.a = tmp;
        } else {
            self.write_ram(self.addr_abs, tmp);
        }

        0
    }

    // rotate right, save in the a register or in ram on the fetched address
    pub fn ror(&mut self) -> u8 {
        self.fetch();
        let tmp = (self.get_flag(FLAGS6502::C) << 7) as u16 | (self.fetched >> 1) as u16;
        self.set_flag(FLAGS6502::C, (self.fetched & 0x01) != 0);
        self.set_flag(FLAGS6502::Z, (tmp & 0x00FF) == 0x00);
        self.set_flag(FLAGS6502::N, (tmp & 0x0080) != 0);
        if self.get_instruction(self.opcode).addrmode_name == *"IMP" {
            self.a = (tmp & 0x00FF) as u8;
        } else {
            self.write_ram(self.addr_abs, (tmp & 0x00FF) as u8);
        }

        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should shift left the bits retrieved from a and set C,N,Z flags
    fn test_asl_instruction_implied() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x0A, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.asl();

        assert_eq!(cpu.a, 0x0E);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should shift left the bits retrieved from memory and set C,N,Z flags
    fn test_asl_instruction_zero_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x06, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.asl();

        assert_eq!(cpu.read_ram(0x00A0), 0x0E);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should shift right the bits retrieved from a and set C,N,Z flags
    fn test_lsr_instruction_implied() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x4A, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.lsr();

        assert_eq!(cpu.a, 0x43);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should shift right the bits retrieved from memory and set C,N,Z flags
    fn test_lsr_instruction_zero_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x46, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.lsr();

        assert_eq!(cpu.read_ram(0x00A0), 0x43);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should rotate left the bits retrieved from a and set C,N,Z flags
    fn test_rol_instruction_implied() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x2A, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.rol();

        assert_eq!(cpu.a, 0x0E);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should rotate left the bits retrieved from memory and set C,N,Z flags
    fn test_rol_instruction_zero_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x26, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.rol();

        assert_eq!(cpu.read_ram(0x00A0), 0x0E);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should rotate right the bits retrieved from a and set C,N,Z flags
    fn test_ror_instruction_implied() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x6A, [0xA0, 0x00]);
        cpu.a = 0x87;

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.imp();
        cpu.ror();

        assert_eq!(cpu.a, 0x43);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }

    #[test]
    // it should rotate right the bits retrieved from memory and set C,N,Z flags
    fn test_ror_instruction_zero_page() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x66, [0xA0, 0x00]);
        cpu.write_ram(0x00A0, 0x87);

        cpu.set_flag(FLAGS6502::N, true);
        cpu.set_flag(FLAGS6502::Z, true);

        cpu.zp0();
        cpu.ror();

        assert_eq!(cpu.read_ram(0x00A0), 0x43);
        assert_eq!(cpu.get_flag(FLAGS6502::Z), 0);
        assert_eq!(cpu.get_flag(FLAGS6502::N), 0);

        Ok(())
    }
}
