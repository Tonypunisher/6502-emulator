use super::CPU;
use super::FLAGS6502;
use super::STACK_ADDR;

// system function implementation
impl CPU {
    pub fn brk(&mut self) -> u8 {
        self.pc += 1;

        // update the interrupt status
        self.set_flag(FLAGS6502::I, true);
        // add pc to the stack
        self.write_ram(
            STACK_ADDR + self.stk_p as u16,
            ((self.pc >> 8) & 0x00FF) as u8,
        );
        self.stk_p -= 1;
        self.write_ram(STACK_ADDR + self.stk_p as u16, (self.pc & 0x00FF) as u8);
        self.stk_p -= 1;

        // update the status
        // and add the status to the stack
        self.set_flag(FLAGS6502::B, true);
        self.write_ram(STACK_ADDR + self.stk_p as u16, self.status);
        self.stk_p -= 1;
        self.set_flag(FLAGS6502::B, false);

        let lo = self.read_ram(0xFFFE) as u16;
        let hi = self.read_ram(0xFFFE + 1) as u16;
        self.pc = (hi << 8) | lo;

        0
    }

    // return from interrupt
    pub fn rti(&mut self) -> u8 {
        self.stk_p += 1;
        let tmp_status_b = self.get_flag(FLAGS6502::B) == 0;
        let tmp_status_u = self.get_flag(FLAGS6502::U) == 0;
        self.status = self.read_ram(STACK_ADDR + self.stk_p as u16);
        self.set_flag(FLAGS6502::B, tmp_status_b);
        self.set_flag(FLAGS6502::U, tmp_status_u);

        self.stk_p += 1;
        self.pc = self.read_ram(STACK_ADDR + self.stk_p as u16) as u16;
        self.stk_p += 1;
        self.pc |= (self.read_ram(STACK_ADDR + self.stk_p as u16) as u16) << 8;

        0
    }

    // no operation
    pub fn nop(&mut self) -> u8 {
        0
    }
}

// unit tests for this module
#[cfg(test)]
mod tests {
    use super::FLAGS6502;
    use super::*;
    use crate::bus::Bus;

    // ###### describe: instructions functions ######

    // utils function: init a CPU and a program with one opcode and operand address
    fn init_test_program(opcode: u8, data: [u8; 2]) -> CPU {
        let bus = Bus::init();
        let mut cpu = CPU::init(bus);

        // we will init the program at 0x8000
        cpu.write_ram(0xFFFC, 0x00);
        cpu.write_ram(0xFFFD, 0x80);

        // init the program from PC adrress
        cpu.write_ram(0x8000, opcode);
        // low address byte at 0 index
        cpu.write_ram(0x8001, data[0]);
        cpu.write_ram(0x8002, data[1]);

        cpu.reset();

        // init struct opcode because we don't run any clock cycle yet on these tests
        cpu.opcode = opcode;
        // increment pc
        cpu.pc += 1;

        cpu
    }

    #[test]
    // it should force an interruption
    fn test_cpu_brk() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x00, [0xA0, 0x00]);
        cpu.write_ram(0xFFFE, 0x00);
        cpu.write_ram(0xFFFF, 0x90);

        cpu.brk();

        // check we save pc on the stack
        // high byte at 0x01FD
        // low byte at 0x01FC
        // after the program init, the pc is at 0x8001
        // and the brk increment it, it is 0x8002 now
        assert_eq!(cpu.read_ram(0x01FD), 0x80);
        assert_eq!(cpu.read_ram(0x01FC), 0x02);

        // check if the status register in on the stack and is updated
        // it is writed just after the pc
        // NVUBDIZC is the order of the flags in the byte
        // the status have the value 00101100 = 0x34
        assert_eq!(cpu.read_ram(0x01FB), 0x34);

        // check B was reseted
        assert_eq!(cpu.get_flag(FLAGS6502::B), 0);
        // but I was setted
        assert_eq!(cpu.get_flag(FLAGS6502::I), 1);

        // check the pc has been reinitialized from 0xFFFE
        assert_eq!(cpu.pc, 0x9000);
        Ok(())
    }

    #[test]
    // it should reset cpu state after an interruption
    fn test_cpu_rti() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0x40, [0xA0, 0x00]);
        cpu.write_ram(0xFFFA, 0x00);
        cpu.write_ram(0xFFFB, 0x90);
        // be sure we can interupt the cpu
        cpu.set_flag(FLAGS6502::I, false);

        cpu.irq();

        // if we change b status, it will be kept when we reset
        cpu.set_flag(FLAGS6502::B, false);

        cpu.imp();
        cpu.rti();

        assert_eq!(cpu.get_flag(FLAGS6502::I), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::B), 1);
        assert_eq!(cpu.get_flag(FLAGS6502::U), 0);
        assert_eq!(cpu.pc, 0x8001);

        Ok(())
    }

    #[test]
    // it do nothing and increment pc
    fn test_cpu_nop() -> Result<(), String> {
        // init context and data
        let mut cpu = init_test_program(0xEA, [0xA0, 0x00]);

        cpu.imp();
        cpu.nop();

        // check the cpu state not changed
        assert_eq!(cpu.a, 0x00);
        assert_eq!(cpu.x, 0x00);
        assert_eq!(cpu.y, 0x00);

        assert_eq!(cpu.stk_p, 0xFD);

        // all status are 0 execept U flag
        // in base 2: 0010 0000
        // to u8    :   2    0
        assert_eq!(cpu.status, 0x20);

        // check debug elements
        assert_eq!(cpu.addr_rel, 0x0000);
        assert_eq!(cpu.addr_abs, 0x0000);
        assert_eq!(cpu.fetched, 0x00);
        // the opcode of nop is setted
        assert_eq!(cpu.opcode, 0xEA);

        assert_eq!(cpu.pc, 0x8001);
        Ok(())
    }
}
