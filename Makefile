DOCKER = docker run -it -v ${PWD}:/workspace/ -v ${HOME}/.gitconfig:${HOME}/.gitconfig -w /workspace/ --rm rust-tools

default: run

style:	## run code formater and linter.
	${DOCKER} cargo fmt
	${DOCKER} cargo clippy

build-docker-image:	## build rust-tools docker image to run this project
	docker build -t rust-tools ./Docker/

build: style	## compile the project with all optimizations.
	${DOCKER} cargo build --release

run: style	## compile and run the project.
	${DOCKER} cargo run

test: style	## run all tests for the project.
	${DOCKER} cargo test

cmd:	## run a command in the docker container. example: 'make cmd cmd="rustc src/main.rs"'.
	${DOCKER} ${cmd}

help:	## show this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: style build-docker-image build run cmd help
.SILENT: style build-docker-image build run cmd help
